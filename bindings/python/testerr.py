#!/usr/bin/env python

import gtask
import glib

loop = glib.MainLoop()

def puts(*a): print ' '.join(a)

t = gtask.Task(lambda: puts("starting") or someBadFunc())
t.add_callback(lambda _: puts('some bad mamo jamo'))
t.add_errback(lambda _: puts('i should be called'))
t.add_callback(lambda _: puts('quitting') or loop.quit())
t.add_errback(lambda _: puts('oops, error not unset?'))

gtask.schedule(t)

loop.run()
