#!/usr/bin/env python

import gtask
import urllib
import gtk
import rss

def get_content(url):
    return urllib.urlopen(url).read()

def parse_content(content):
    p = rss.Parser()
    p.load_from_data(content, len(content))
    doc = p.get_document()
    items = doc.get_items()
    return items

def print_items(items):
    for item in items:
        print item.props.title

t = gtask.Task(get_content, 'http://planet.gnome.org/atom.xml')
t.add_callback(lambda c: gtask.Task(parse_content, c))
#t.add_callback(parse_content)
t.add_callback(print_items)
t.add_callback(lambda _: gtk.main_quit())

gtask.scheduler_get_default().schedule(t)
gtk.main()
