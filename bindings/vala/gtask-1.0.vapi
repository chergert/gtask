/* gtask-1.0.vapi
 *
 * Copyright (C) 2008 Christian Hergert <chris@dronelabs.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 
 * 02110-1301 USA
 */

using GLib;

[Import ()]
[CCode (cprefix="G", lower_case_cprefix="g_", cheader_filename="gtask/gtask.h")]
namespace GTask {
	[CCode (cname = "GTaskFunc")]
	public delegate GTask.Task? TaskFunc (Task task, Value result);

	[CCode (cname = "GTaskCallback")]
	public delegate GTask.Task? TaskCallback (Task task, Value result);

	[CCode (cname = "GTaskErrback")]
	public delegate GTask.Task? TaskErrback (Task task, Error error, Value result);

	[CCode (cname = "GTask")]
	public class Task: Object {
		[CCode (instance_pos = 1)]
		public Task (TaskFunc func, DestroyNotify? notify = null);

		public void add_callback (TaskCallback callback, DestroyNotify? notify = null);
		public void add_errback  (TaskErrback errback, DestroyNotify? notify = null);

		public void add_callback_closure (Closure closure);
		public void add_errback_closure (Closure closure);
	}

	[CCode (cname = "GTaskScheduler")]
	public class TaskScheduler: Object {
		public TaskScheduler ();
		public static TaskScheduler get_default ();

		public virtual void schedule (Task task);

		protected virtual void callback (Task task);
		protected virtual void init ();
	}
}
