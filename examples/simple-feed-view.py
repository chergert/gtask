#!/usr/bin/env python
"""
This is a port of the simple-feed-view.vala to python.  Until
I write python bindings for rss-glib, ill use feedparser here.
"""

import gobject
import gtask
import gtk
import webkit
import rss
import urllib

class SimpleFeedView(object):

    def __init__(self):
        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.window.set_default_size(600, 400)
        self.window.set_title('SimpleFeedView')
        self.window.connect('destroy', gtk.main_quit)
        self.window.show()

        vbox = gtk.VBox()
        self.window.add(vbox)
        vbox.show()

        hbox = gtk.HBox()
        vbox.pack_start(hbox, False, True, 2)
        hbox.show()

        go = gtk.Button(stock = gtk.STOCK_GO_FORWARD)
        go.connect('clicked', self.on_go_clicked)
        go.show()

        self.entry = gtk.Entry()
        self.entry.set_text('http://planet.gnome.org/atom.xml')
        hbox.pack_start(self.entry, True, True, 6)
        hbox.pack_start(go, False, True, 0)
        self.entry.show()

        sw = gtk.ScrolledWindow()
        sw.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        vbox.pack_start(sw, True, True, 0)
        sw.show()

        self.webview = webkit.WebView()
        sw.add(self.webview)
        self.webview.show()

    def on_go_clicked(self, button):
        print 'go clicked, lets start'
        url = self.entry.get_text()

        def parse(data):
            print 'parsing data'
            p = rss.Parser()
            p.load_from_data(data, len(data))
            d = p.get_document()
            return d.get_items()

        def build(items):
            print 'building html'
            return ''.join([
                '<h1>%s</h1><p>%s</p>' % (i.props.title, i.props.description)
                for i in items])

        task = gtask.Task(lambda: urllib.urlopen(url).read())
        task.add_callback(lambda data: gtask.Task(parse, data))
        task.add_callback(lambda items: gtask.Task(build, items))
        task.add_callback(lambda data: self.webview.load_html_string(data, url))
        gtask.scheduler_get_default().schedule(task)

if __name__ == '__main__':
    v = SimpleFeedView()
    gtk.main()
