/* simple-feed-view.vala
 *
 * Copyright (C) 2008 Christian Hergert <chris@dronelabs.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 
 * 02110-1301 USA
 */

/* This has not been updated to the newest vala. it works fine
 * with 0.3.5, but 0.5.1 generates some g_object_unref warnings
 * at runtime.
 */

using GLib;
using Gtk;
using WebKit;
using Rss;
using GTask;

namespace SimpleFeedView {
	public class Main: GLib.Object {
		Window window;
		Entry entry;
		Document document;
		string url_base;
		string generated;
		string content;
		ulong content_length;
		WebView webview;

		construct {
			window = new Gtk.Window (Gtk.WindowType.TOPLEVEL);
			window.set_default_size (600, 400);
			window.destroy += Gtk.main_quit;
			window.show ();

			var vbox = new Gtk.VBox (false, 0);
			window.add (vbox);
			vbox.show ();

			var hbox = new Gtk.HBox (false, 0);
			vbox.pack_start (hbox, false, true, 2);
			hbox.show ();

			var go = new Gtk.Button.from_stock (Gtk.STOCK_GO_FORWARD);

			entry = new Gtk.Entry ();
			entry.set_text ("http://planet.gnome.org/atom.xml");
			hbox.pack_start (entry, true, true, 6);
			entry.show ();

			var sw = new Gtk.ScrolledWindow (null, null);
			sw.set_policy (Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC);
			vbox.pack_start (sw, true, true, 0);
			sw.show ();

			webview = new WebView ();
			sw.add (webview);
			webview.show ();

			go.clicked += _ => {
				/* store the url to prevent race */
				url_base = entry.get_text ();

				/* asynchronously get the url */
				var task = new Task (this.retrieve_url);

				/* create another task to proces it */
				task.add_callback (_ => {
					return new Task (this.build_content);
				});

				/* turn the document into a succinct overview */
				task.add_callback (_ => {
					return new Task (this.convert);
				});

				/* load the new data into the webview */
				task.add_callback (_ => {
					debug ("loading webview");

					if (webview == null || url_base == null)
						return null;

					if (generated != null)
						webview.load_html_string (generated, url_base);

					return null;
				});

				TaskScheduler.get_default ().schedule (task);
			};

			hbox.pack_start (go, false, true, 6);
			go.show ();
		}

		Task? retrieve_url (Task task, Value result) {
			debug ("retrieving url '%s'", url_base);
			try {
				if (url_base == null)
					return null;

				string etag;

				var file = Vfs.get_default ().get_file_for_uri (url_base);
				file.load_contents (null, out this.content, out content_length, out etag);
			}
			catch (Error error) {
				warning ("Error retrieving data");
				warning ("%s", error.message);
			}
			return null;
		}

		Task? build_content (Task task, Value result) {
			debug ("Building content for '%s'", url_base);
			var parser = new Parser ();
			try {
				if (content != null && content.length > 0)
				{
					parser.load_from_data (content, content_length);
					document = parser.get_document ();
					debug ("document parsed");
				}
			}
			catch {
				debug ("Could not parse data");
				document = null;
			}
			return null;
		}

		Task? convert (Task task, Value result) {
			var sb = new StringBuilder ();

			if (document == null)
				return null;

			foreach (var item in document.get_items ()) {
				sb.prepend ("<h1>%s</h1><h3>%s %s</h3><p>%s</p>".printf (
				            item.title,
						    item.author,
						    item.pub_date,
				            item.description));
			}

			document = null;
			this.generated = sb.str;
			return null;
		}
	}

	const OptionEntry[] options = {
		{ null }
	};

	static int main (string[] args) {
		if (!Thread.supported ())
			error ("Threads are not supported on your platform!");

		try {
			var context = new OptionContext ("- a simple rss viewer");
			context.set_help_enabled (true);
			context.add_main_entries (options, null);
			context.add_group (Gtk.get_option_group (true));
			context.parse (ref args);
		}
		catch (OptionError error) {
			stdout.printf ("%s\n", error.message);
			stdout.printf ("Run '%s --help' for command list.\n", args[0]);
			return 1;
		}

		Environment.set_application_name ("SimpleFeedView");
		Gtk.init (ref args);
		var m = new Main ();
		Gtk.main ();

		return 0;
	}
}
