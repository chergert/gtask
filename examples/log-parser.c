#include <stdlib.h>

#include <glib.h>
#include <gtask.h>
#include <gtaskscheduler.h>

static gchar          *filename  = NULL;
static GTaskScheduler *scheduler = NULL;
static GMainLoop      *main_loop = NULL;
static gint            todo      = 0;

static GTask* quit  (GTask *task, GValue *result, gpointer user_data);
static GTask* parse (GTask *task, GValue *result, gpointer user_data);

static GOptionEntry entries[] =
{
	{ "filename", 'f', 0, G_OPTION_ARG_FILENAME, &filename, "Logfile to parse", "FILE" },
	{ NULL }
};

int
main (int argc, char *argv[])
{
	GError         *error     = NULL;
	GOptionContext *context   = NULL;
	GTask          *task      = NULL;

	context = g_option_context_new ("- gtask example logfile parser");
	g_option_context_add_main_entries (context, entries, NULL);

	if (!g_option_context_parse (context, &argc, &argv, &error))
	{
		g_print ("option parsing failed: %s\n", error->message);
		g_error_free (error);
		return 1;
	}

	/* initialize gobject and gthread */
	g_type_init ();
	g_thread_init (NULL);

	/* create our main loop */
	main_loop = g_main_loop_new (NULL, FALSE);

	/* create our task scheduler */
	scheduler = g_task_scheduler_new ();

	/* do our work from a task, so we can run a main loop */
	task = g_task_new (parse, filename, g_free);

	/* scheduler our task for execution */
	g_task_scheduler_schedule (scheduler, task);

	/* run the main loop */
	g_main_loop_run (main_loop);

	return 0;
}

static int month (const char *data)
{
	if (g_str_has_prefix (data, "Jan")) {
		return 1;
	}
	else if (g_str_has_prefix (data, "Feb")) {
		return 2;
	}
	else if (g_str_has_prefix (data, "Mar")) {
		return 3;
	}
	else if (g_str_has_prefix (data, "Apr")) {
		return 4;
	}
	else if (g_str_has_prefix (data, "May")) {
		return 5;
	}
	else if (g_str_has_prefix (data, "Jun")) {
		return 6;
	}
	else if (g_str_has_prefix (data, "Jul")) {
		return 7;
	}
	else if (g_str_has_prefix (data, "Aug")) {
		return 8;
	}
	else if (g_str_has_prefix (data, "Sep")) {
		return 9;
	}
	else if (g_str_has_prefix (data, "Oct")) {
		return 10;
	}
	else if (g_str_has_prefix (data, "Nov")) {
		return 11;
	}
	else if (g_str_has_prefix (data, "Dec")) {
		return 12;
	}
	return -1;
}

static GTask*
parse_line (GTask *task, GValue *value, gpointer user_data)
{
	char *line = user_data;
	int   mon  = 0;
	int   day  = 0;
	int   hour = 0;
	int   min  = 0;
	int   sec  = 0;

	mon = month (line);
	line = line + 4;

	line[2] = '\0';
	day = atoi (line);
	line = line + 3;

	line[2] = '\0';
	hour = atoi (line);
	line = line + 3;

	line[2] = '\0';
	min = atoi (line);
	line = line + 3;

	line[2] = '\0';
	sec = atoi (line);
	line = line + 3;

	/* would be cool to do something with the parsed data here :-) */
	g_debug ("%d-%d %02d:%02d:%02d", mon, day, hour, min, sec);

	/* decrement our todo count, and return the quit task if we are done */
	if (g_atomic_int_dec_and_test (&todo))
		g_task_scheduler_schedule (g_task_scheduler_get_default (),
		                           g_task_new (quit, NULL, NULL));

	return NULL;
}

static GTask*
parse (GTask *task, GValue *result, gpointer user_data)
{
	const gchar *fname = user_data;

	todo = 1;

	if (fname)
	{
		g_debug ("Parsing %s ...", fname);

		GIOChannel *stream = g_io_channel_new_file (fname, "r", NULL);
		gchar *line = NULL;

		if (stream)
		{
			while (G_IO_STATUS_NORMAL ==
				g_io_channel_read_line (stream, &line,
				                        NULL, NULL, NULL))
			{
				/* keep track of how many lines to do */
				g_atomic_int_inc (&todo);

				/* schedule the line */
				g_task_scheduler_schedule (
					scheduler,
					g_task_new (parse_line, line, g_free));
			}
		}

		g_io_channel_close (stream);
	}

	if (g_atomic_int_dec_and_test (&todo))
		g_main_loop_quit(main_loop);

	return NULL;
}

static GTask*
quit (GTask *task, GValue *result, gpointer user_data)
{
	g_main_loop_quit (main_loop);
	return NULL;
}
