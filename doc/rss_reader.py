#!/usr/bin/env python

import glib    # For glib main loop
import gtask   # Our tasking library
import rss     # For rss parsing
import urllib  # To download the url

url = 'http://audidude.com/blog/?feed=rss2'
loop = glib.MainLoop()

def parse(data):
    p = rss.Parser()
    p.load_from_data(data, len(data))
    l = p.get_document().get_items()
    l.reverse() # order newest article first
    return l

def printit(i):
    print 'Title:   %s' % i.props.title
    print 'Author:  %s' % i.props.author
    print 'Date:    %s' % i.props.pub_date
    print ''

# download the url as an async task
task = gtask.Task(lambda: urllib.urlopen(url).read())

# process the content into a list of rss articles.
# the result of the task will be the first argument to parse().
task.add_callback(parse)

# print each article to the command line
task.add_callback(lambda l: [printit(i) for i in l])

# quit the main loop when we are done
task.add_callback(lambda _: loop.quit())

# schedule the task for execution
gtask.schedule(task)

loop.run()
