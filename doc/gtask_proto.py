class Task(GObject):
    def execute(self):
        """
        Performs the actual task.  Subclasses may override this
        method or simply set the task delegate and it will be
        executed for you.
        """
    def cancel(self):
        """
        Cancels the task.  It is up to the execute() method to
        check the state to see if it has been cancelled.
        """
    def add_callback(self, callback, *args):
        """
        Adds a new handler to the post-processing chain.  The handler 
        is a callback type which means the task or previous handler
        must have returned cleanly for the callback to be called.

        callback is called with the extra arguments passed.
        """
    def add_errback(self, errback, *args):
        """
        Adds a new handler to the post-processing chain.  The handler
        is a errback type which means the task, previous callback, or
        previous errback must have returned uncleanly, leaving an error
        for the current state of the task.

        See also: g_task_set_error().
        """
    def add_dependency(self, task):
        """
        Adds a task as a dependency for the task's execution.  This
        means that the task will continue to be in the G_TASK_WAITING
        state until the dependent tasks have completed.
        """
    def remove_dependency(self, task):
        """
        Removes a task from being a dependency for the task.  If
        no more dependencies exist and the task has previously been
        scheduled, it will be executed.
        """
