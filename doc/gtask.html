<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<META NAME="generator" CONTENT="http://txt2tags.sf.net">
<LINK REL="stylesheet" TYPE="text/css" HREF="gtask.css">
<TITLE>Asynchronous Programming with GTask</TITLE>
</HEAD><BODY BGCOLOR="white" TEXT="black">
<P ALIGN="center"><CENTER><H1>Asynchronous Programming with GTask</H1>
<FONT SIZE="4">
<I>Christian Hergert &lt;chris@dronelabs.com&gt;</I><BR>
Last Updated: Wednesday November 12, 2008
</FONT></CENTER>

<P></P>
<HR NOSHADE SIZE=1>
<P></P>
  <UL>
  <LI><A HREF="#Introduction">Introduction</A>
  <LI><A HREF="#Tasks">Tasks</A>
    <UL>
    <LI><A HREF="#toc3">What's in a Task?</A>
    <LI><A HREF="#Processing_Chain">Processing Chains</A>
      <UL>
      <LI><A HREF="#Callbacks_and_Errbacks">Callbacks and Errbacks</A>
      <LI><A HREF="#toc6">Returning Tasks from Handlers</A>
      <LI><A HREF="#toc7">Closures</A>
      <LI><A HREF="#Main_Dispatch">Main Dispatch</A>
      </UL>
    <LI><A HREF="#toc9">Task Dependencies</A>
    <LI><A HREF="#toc10">Asynchronous Tasks</A>
    </UL>
  <LI><A HREF="#Task_Scheduler">Task Scheduler</A>
    <UL>
    <LI><A HREF="#toc12">Custom Schedulers</A>
    </UL>
  <LI><A HREF="#toc13">Tips and Tricks</A>
    <UL>
    <LI><A HREF="#toc14">Avoiding shared state</A>
    </UL>
  <LI><A HREF="#toc15">Roadmap</A>
    <UL>
    <LI><A HREF="#toc16">Remove Locks</A>
    <LI><A HREF="#toc17">Scheduler Revamp</A>
    <LI><A HREF="#toc18">Auto marshaling of values</A>
    </UL>
  <LI><A HREF="#toc19">Examples</A>
    <UL>
    <LI><A HREF="#toc20">Python Rss Viewer</A>
    <LI><A HREF="#toc21">Vala Rss Viewer</A>
    </UL>
  <LI><A HREF="#toc22">Copyright</A>
  </UL>

<P></P>
<HR NOSHADE SIZE=1>
<P></P>
<TABLE CELLPADDING="4">
<TR>
<TD>You can find the API documentation for GTask at <A HREF="http://docs.dronelabs.com/gtask/api/">http://docs.dronelabs.com/gtask/api/</A>.</TD>
</TR>
</TABLE>

<P></P>
<P>
GTask is a young project working towards providing an asynchronous
toolkit to GObject and associated language bindings.  This document
provides an introduction to asynchronous programming using GTask in
its current form.  Where appropriate, I will try to draw a clear line
of what will or may change as development continues.
</P>
<P>
Experienced programmers may see similarities to other concurrent
frameworks as they heavily influenced the design of GTask.  I primarily
took this route so that documentation on concurrent programming from
other projects would continue to be applicable.
</P>
<A NAME="Introduction"></A>
<H1>Introduction</H1>
<P>
Asynchronous programming is on the rise.  It is important to have
a good framework to write this paradigm changing software or code
can get out of hand quite quickly.  GTask is a mini-framework to
simplify asynchronous and concurrent programming with GObject.
Now let me Tarantino for a moment and explain why there has been
such a shift to asynchronous programming.
</P>
<P>
Whether you are writing a small script to manage your mp3's or a
distributed map-reduce to parse your log files, chances are you
will run into similar problems in software design.  Computing
is not instantaneous.  Sometimes we are CPU bound while waiting
for a batch of processing to complete.  Sometimes we are waiting
for data to arrive from an external resource.  However, the problem
can be generalized the same.  We make a request, and at some point
in the future it will be done.
</P>
<P>
GTask works to simplify this problem by abstracting this into a
concept called Task's.  A task has a specific set of processing
to perform or perhaps data to receive.  Any of the steps may
take an unknown period of time, so a series of callbacks and
errbacks are performed after the task has completed.  Don't worry
if you have no idea what those are.  They will be explained in more
depth in <A HREF="#Callbacks_and_Errbacks">Callbacks and Errbacks</A>.
</P>
<P>
This type of programming is not very new in fact.  It has been done
for years.  You might also know it as <B>event-driven</B> or
<B>callback-based</B> programming.  It is my sincere hope, that even
if GTask isn't for you, that you find the absolute joy that comes
with mastering asynchronous programming.  Once mastered, it is
almost impossible to give up.
</P>
<P>
I would like to thank the developers of Python Twisted for providing
what is quite possibly the best implementation of an async framework
in existence and a plethora of networking protocols to go with it.
Twisted became my drug of choice rather quick and I miss it in every
other language I use. Hence, GTask.
</P>
<A NAME="Tasks"></A>
<H1>Tasks</H1>
<P>
If GTask was a race, the Task object would be the starting line.
It encapsulates a work-item to provide common functionality such as
task cancellation, processing chains, and execution dependencies.
</P>
<A NAME="toc3"></A>
<H2>What's in a Task?</H2>
<P>
<A HREF="http://docs.dronelabs.com/gtask/api/GTask.html">GTask</A> is a <I>GObject</I> subclass meaning you
may inherit and override features as you choose.  Lets start off by
creating a new instance of a GTask and then cover the details that
go into making that happen.  The examples provided throughout the
document are in C, however, you will find a few supplements at the
end exemplifying the language bindings.
</P>
<P>
You will need to include the <I>&lt;gtask/gtask.h&gt;</I> header in your sources
files as such.
</P>
<div style="background:#ffffff;color:#000000;"><font face="monospace">
<font color="#a52a2a">1 </font><font color="#a020f0">#include </font><font color="#ff00ff">&lt;gtask/gtask.h&gt;</font><br></font></div>
<P>
For our first example we will create a task which encapsulates a
blocking file read from disk.  While this isn't ideal in true
asynchronous programming, it does demonstrate how you can use GTask
to push long blocking or cpu intensive calls to a thread pool for
execution.
</P>
<div style="background:#ffffff;color:#000000;"><font face="monospace">
<font color="#a52a2a">&nbsp;1 </font><font color="#0000ff">static</font>&nbsp;GValue*<br>
<font color="#a52a2a">&nbsp;2 </font>read_contents (GTask *self, gpointer user_data)<br>
<font color="#a52a2a">&nbsp;3 </font>{<br>
<font color="#a52a2a">&nbsp;4 </font>&nbsp;&nbsp;<font color="#0000ff">const</font>&nbsp;<font color="#0000ff">char</font>&nbsp;*filename = user_data;<br>
<font color="#a52a2a">&nbsp;5 </font>&nbsp;&nbsp;<font color="#ff0000">// ...</font><br>
<font color="#a52a2a">&nbsp;6 </font>&nbsp;&nbsp;<font color="#a52a2a">return</font>&nbsp;<font color="#ff00ff">NULL</font>;<br>
<font color="#a52a2a">&nbsp;7 </font>}<br>
<font color="#a52a2a">&nbsp;8 </font><br>
<font color="#a52a2a">&nbsp;9 </font><font color="#0000ff">int</font><br>
<font color="#a52a2a">10 </font>main (<font color="#0000ff">int</font>&nbsp;argc, <font color="#0000ff">char</font>&nbsp;*argv[])<br>
<font color="#a52a2a">11 </font>{<br>
<font color="#a52a2a">12 </font>&nbsp;&nbsp;GTask *task = g_task_new (read_contents, <font color="#ff00ff">&quot;/etc/passwd&quot;</font>, <font color="#ff00ff">NULL</font>);<br>
<font color="#a52a2a">13 </font>&nbsp;&nbsp;<font color="#ff0000">// ...</font><br>
<font color="#a52a2a">14 </font>&nbsp;&nbsp;<font color="#a52a2a">return</font>&nbsp;<font color="#ff00ff">0</font>;<br>
<font color="#a52a2a">15 </font>}<br>
</font></div>
<P>
You will notice we didn't really do anything here as the purpose
was mostly so that you will see line 12.  Here we create a new
instance of the <A HREF="http://docs.dronelabs.com/gtask/api/GTask.html">GTask</A> class.  We have
provided a callback method matching the signature of a <B>GTaskFunc</B>
which will be called when the task is executed on a worker thread.
Our second argument, <I>"/etc/passwd"</I> is the user data which is
passed to <I>read_contents()</I>.  The third parameter, which we
did not use, is a callback for when the task loses all references
and will be cleaned up.  You will typically use this to lower the
reference count on the second parameter if needed.
</P>
<P>
Lets look at the signature of GTaskFunc.
</P>
<PRE>
  GValue* (*GTaskFunc) (GTask *task, gpointer user_data);
</PRE>
<P></P>
<P>
The first argument to the delegate is the task which is being
invoked.  You can use this during runtime for various effects
on the task.  For example, you can set the task as errored using
<A HREF="http://docs.dronelabs.com/gtask/api/GTask.html#g-task-set-error">g_task_set_error()</A>.
The second parameter, as explained above, is the <I>user_data</I>
argument supplied when creating the task.
</P>
<P>
With a task created, we can start digging into the real power
of GTask, the <A HREF="Processing_Chain">Processing Chain</A>.
</P>
<A NAME="Processing_Chain"></A>
<H2>Processing Chains</H2>
<P>
The <I>processing chain</I> in GTask is a series of
<A HREF="Callbacks_and_Errbacks">Callbacks and Errbacks</A> that occur after
the execution of a task.  This allows for post-analysis, further
processing, and error handling.  All of this is centered around
manipulating the data generated during the <I>GTaskFunc</I>.
</P>
<P>
For example, if our <I>GTaskFunc</I> in the first example was to
return the string of data read from the file, it would be common
for callbacks to do further processing on the content.  Perhaps
cutting out some data, or adding additional.  Likewise, if there
was an error during processing, errbacks could be called to
resolve the error.
</P>
<A NAME="Callbacks_and_Errbacks"></A>
<H3>Callbacks and Errbacks</H3>
<P>
Before we look into the process of how <I>Callbacks and Errbacks</I>
are executed, lets take a look at what each are intended to perform.
</P>
<P>
A <B>callback</B> is intended to manipulate the data further after the
task has completed.
</P>
<P>
An <B>Errback</B> is intended to resolve an error that happened further
up in the processing chain.
</P>
<P>
In the following image, you will see how callbacks and errbacks are
part of a system called <I>Task Handlers</I>.  Handlers are not exposed
to the public api but serve the point of exemplifying control flow
in the processing chain.
</P>
<P>
In the following diagram you can see how each handler has a Callback
and an Errback.  A handler is not required to have both, but it allows
for branch control during post processing.  Handlers are executed
sequentially after the task.  If the task currently has an error, the
next available errback will be executed.  If the task is not errored,
the next available callback will be executed.  This continues through
the chain until all handlers are executed.
</P>
<P>
<IMG ALIGN="middle" SRC="processing-chain.png" BORDER="0" ALT="">
</P>
<P>
When I wrote that the handlers are executed sequentially, I was lying
to a degree.  While they are guaranteed to be executed one after another,
there may be a degree of time between the handlers.  This is because we
provide a mechanism to defer your callback or errback to be executed in
a separate thread.  A GUI thread for example.  In this case, the processing
chain pauses until the callback or errback completes.  We will go into
this deeper in <A HREF="Main_Dispatch">Main Dispatch</A>.
</P>
<A NAME="toc6"></A>
<H3>Returning Tasks from Handlers</H3>
<P>
What happens if you need to block the task until a secondary task has
completed?  You may return a new task from a <I>GTaskFunc</I>,
<I>GTaskCallback</I>, or <I>GTaskErrback</I>.  Doing so will pause any
further execution of the processing chain until the secondary task has
completed.  The result of the secondary task will become the result of
the original task when execution of the processing chain continues.
Likewise, if the secondary task completes with an error, that error
will become the error for the original task.
</P>
<A NAME="toc7"></A>
<H3>Closures</H3>
<P>
GTask internally uses GClosure to manage execution of delegates.  This
is to both simplify language bindings and the internal code.  The power
of doing so can be seen in the python bindings where delegate methods
are free to take an arbitrary number of parameters.  Programmers in C are
currently not this lucky.  If anyone reading this has ideas on integrating
variable arguments into the C closures, patches are welcome.
</P>
<P>
In addition to using delegates directly, methods are provided to
pass GClosure instances.  See the documentation on the following methods
for more information.
</P>
<UL>
<LI><A HREF="http://docs.dronelabs.com/gtask/api/GTask.html#g-task-new-from-closure">g_task_new_from_closure()</A>
<LI><A HREF="http://docs.dronelabs.com/gtask/api/GTask.html#g-task-add-callback-closure">g_task_add_callback_closure()</A>
<LI><A HREF="http://docs.dronelabs.com/gtask/api/GTask.html#g-task-add-errback-closure">g_task_add_errback_closure()</A>
<LI><A HREF="http://docs.dronelabs.com/gtask/api/GTask.html#g-task-add-both-closure">g_task_add_both_closure()</A>
</UL>

<A NAME="Main_Dispatch"></A>
<H3>Main Dispatch</H3>
<P>
The <I>Main Dispatch</I> feature of GTask provides a mechanism for callbacks
and errbacks to be executed on the main thread.  This saves programmers the
need to worry about having control over the GDK thread lock for gtk+.  This
functionality is enabled by default.  To disable it, set the
<I>"main-dispatch"</I> property to <I>FALSE</I> on your
<A HREF="http://docs.dronelabs.com/gtask/api/GTaskScheduler.html">GTaskScheduler</A>.
</P>
<PRE>
  g_object_set (scheduler, "main-dispatch", FALSE, NULL);
</PRE>
<P></P>
<P>
A common use of the main dispatch feature is to generate new content for
your GUI in a GTask and update the GUI from the callback.  If there is
potential for an error, the errback is a great place to update an error
dialog for the GUI.
</P>
<A NAME="toc9"></A>
<H2>Task Dependencies</H2>
<P>
<A HREF="http://docs.dronelabs.com/gtask/api/GTask.html">GTask</A>'s may have dependencies that prevent premature
execution of the task.  By adding a dependency to a task, the scheduler is
not allowed to execute the task until those dependencies have been met.  This
is a great way to create a one-to-many processing notification where many
tasks are dependent on a single shared task completing.
</P>
<P>
For documentation on using dependencies, see the API reference for the
following methods.
</P>
<UL>
<LI><A HREF="http://docs.dronelabs.com/gtask/api//GTask.html#g-task-add-dependency">g_task_add_dependency()</A>
<LI><A HREF="http://docs.dronelabs.com/gtask/api//GTask.html#g-task-remove-dependency">g_task_remove_dependency()</A>
</UL>

<P>
In the near future, helper methods utilizing dependencies will be added.
Currently, I'm considering the following helper methods.
</P>
<PRE>
  GTask* g_task_any_of (GTask *task1, ...);
</PRE>
<P></P>
<P>
This method could be used to execute a task when any of the dependent
tasks complete.
</P>
<PRE>
  GTask* g_task_all_of (GTask *task1, ...);
</PRE>
<P></P>
<P>
This method could be used to execute a task when all of the dependent
tasks have completed.
</P>
<PRE>
  GTask* g_task_n_of (int n, GTask *task1, ...);
</PRE>
<P></P>
<P>
This method could be used to execute a task when a given number of
dependent tasks have completed.
</P>
<A NAME="toc10"></A>
<H2>Asynchronous Tasks</H2>
<P>
As I mentioned in the beginning, simply putting many blocking calls
on a thread pool is not the answer.  It simply hides the problem for a short
period of time.  As you scale, you will often run into new, more challenging
debugging problems.
</P>
<P>
Before I start, I should mention that I'm not entirely happy with this portion
of GTask yet, and am looking for insight on how we can make it more friendly
and desirable to use.
</P>
<P>
When a task is utilizing asynchronous methods in its <I>GTaskFunc</I>, we cannot
know that the task has actually completed when the execution completes on the
worker thread.  This is because the callback for the asynchronous call the
delegate did most likely hasn't completed yet.  Therefore, a task can be
declared as asynchronous using
<A HREF="http://docs.dronelabs.com/gtask/api/GTask.html#g-task-set-async">g_task_set_async()</A>.  Asynchronous
tasks carry the burden of notifying the task when their execution has
completed.
</P>
<P>
This is done as follows.
</P>
<PRE>
  g_task_set_state (mytask, G_TASK_CALLBACKS);
</PRE>
<P></P>
<P>
This moves the tasks state into the callbacks phase, which is synonymous to
the post processing chain.
</P>
<A NAME="Task_Scheduler"></A>
<H1>Task Scheduler</H1>
<P>
The task scheduler manages how tasks get executed during runtime.  It is
currently monolithic in nature, requiring much to be reimplemented if you
choose to subclass it.  I do hope to change this in the near future.  With
that said, there is nothing preventing you from writing your own scheduling
mechanism for the problem domain at hand.  This section will be fairly light
until the revamp has been completed.
</P>
<P>
To those considering implementing a scheduler: I suggest that if tasks,
callbacks, or errbacks return a task, you immediately execute the task
to prevent a sort of bumper-to-bumper effect in the scheduler.
</P>
<P>
Another pain point I have which I'm not sure I currently have a solution
for is scheduling of tasks.  Twisted for example, does not have any concept
of task scheduling.  Its built into a single main loop called the reactor.
While this is nice and simple, I do feel it holds back a bit of control
that GTask currently provides with the ability for multiple schedulers.
</P>
<P>
To schedule a task using the default scheduler, we use the following.
</P>
<PRE>
  GTaskScheduler *sched = g_task_scheduler_get_default ();
  g_task_scheduler_schedule (sched, mytask);
</PRE>
<P></P>
<A NAME="toc12"></A>
<H2>Custom Schedulers</H2>
<P>
To implement your own scheduler, you will probably want to override two
functions in the vtable.  The <I>GTaskSchedulerClass::init</I> method and
the <I>GTaskScheduler::schedule</I> method.
</P>
<P>
The default implementation creates a thread pool in the init method
and pushes a work item onto the thread pool in the schedule method.
You should really check out the <A HREF="http://git.dronelabs.com/?p=users/chris/gtask.git;a=blob;f=gtask/gtaskscheduler.c;hb=HEAD">source</A> for the details.
</P>
<A NAME="toc13"></A>
<H1>Tips and Tricks</H1>
<A NAME="toc14"></A>
<H2>Avoiding shared state</H2>
<P>
Many times the slowdown in applications comes from negotiating locks and
the cost of memory barriers.  The processing chain provides an excellent
way to mitigate this problem because your state is passed from handler
to handler and can be safe from other threads if you so choose.
</P>
<P>
GTask currently does have a few GMutex's used and a few race conditions
have not been fixed.  As development continues I hope to remove the locks
in favor of lockless algorithms and that goes hand-in-hand with fixing
the remaining race conditions.  I chose to go the route of defining the
API before really pushing the code perfection.
</P>
<A NAME="toc15"></A>
<H1>Roadmap</H1>
<P>
There are so many things I'd like to do with this library.  Here is my
short list.
</P>
<A NAME="toc16"></A>
<H2>Remove Locks</H2>
<UL>
<LI>Go through the life of a task and look at where communication with
those tasks commonly occur.  For those common paths, we must find a way
to reduce or remove locks.
</UL>

<A NAME="toc17"></A>
<H2>Scheduler Revamp</H2>
<UL>
<LI><B>Throttle scheduling of tasks</B>.  I think it is important to have the
ability in the scheduler to restrict how many tasks may be scheduled per
second/minute/hour.  This could allow for dropping requests past a certain
threshold.
<LI><B>External control of thread management</B>.  I really don't think that
thread management needs to be implemented by each scheduler.  Schedulers
should concentrate on the minimum threads required, the maximum they will
utilize, and the preferred size for their tasks at hand.
</UL>

<A NAME="toc18"></A>
<H2>Auto marshaling of values</H2>
<UL>
<LI>It has long been a goal of mine to have an asynchronous framework
that allows for asynchronous calls to cross the language domain within
a single process space.  Not that it is really needed, it just sounds
fun.  This could be provided by enabling extra marshaling in the closures
that convert higher language objects into native C types.  Python GObject
already makes this really simple.
</UL>

<A NAME="toc19"></A>
<H1>Examples</H1>
<A NAME="toc20"></A>
<H2>Python Rss Viewer</H2>
<P>
This example provides a simple webkit view that allows you to put in the url
of an rss or atom feed and generates a simple html overview of contents.
</P>
<P>
This example can be found <A HREF="http://git.dronelabs.com/?p=users/chris/gtask.git;a=blob;f=examples/simple-feed-view.py;hb=HEAD">here</A>.
</P>
<A NAME="toc21"></A>
<H2>Vala Rss Viewer</H2>
<P>
This example is pretty identical to the python version, except implemented in
Vala.
</P>
<P>
This example can be found <A HREF="http://git.dronelabs.com/?p=users/chris/gtask.git;a=blob;f=examples/simple-feed-view.vala;hb=HEAD">here</A>.
</P>
<A NAME="toc22"></A>
<H1>Copyright</H1>
<P>
Copyright &copy; 2008 Christian Hergert
</P>
<P>
Permission is granted to copy, distribute and/or modify this document under the terms of the GNU Free Documentation License, Version 1.1 or any later version published by the Free Software Foundation with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts. You may obtain a copy of the GNU Free Documentation License from the Free Software Foundation by visiting their Web site or by writing to:
</P>
<P>
The Free Software Foundation, Inc.,
59 Temple Place - Suite 330,
Boston, MA 02111-1307,
USA
</P>

<!-- html code generated by txt2tags 2.3 (http://txt2tags.sf.net) -->
<!-- cmdline: txt2tags -\-target html -\-style gtask.css gtask.t2t -->
</BODY></HTML>
