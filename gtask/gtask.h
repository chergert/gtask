/* gtask.h
 *
 * Copyright (C) 2008 Christian Hergert <chris@dronelabs.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 
 * 02110-1301 USA
 */

#ifndef __G_TASK_H__
#define __G_TASK_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define G_TYPE_TASK_STATE (g_task_state_get_type ())
#define G_TYPE_TASK       (g_task_get_type())

#define G_TASK(obj) (                  \
    G_TYPE_CHECK_INSTANCE_CAST ((obj), \
    G_TYPE_TASK,                       \
    GTask))

#define G_TASK_CLASS(klass) (          \
    G_TYPE_CHECK_CLASS_CAST ((klass),  \
    G_TYPE_TASK,                       \
    GTaskClass))

#define G_IS_TASK(obj) (               \
    G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
    G_TYPE_TASK))

#define G_IS_TASK_CLASS(klass) (       \
    G_TYPE_CHECK_CLASS_TYPE ((klass),  \
    G_TYPE_TASK))

#define G_TASK_GET_CLASS(obj) (        \
    G_TYPE_INSTANCE_GET_CLASS ((obj),  \
    G_TYPE_TASK,                       \
    GTaskClass))

typedef enum
{
	G_TASK_WAITING,
	G_TASK_READY,
	G_TASK_EXECUTING,
	G_TASK_CALLBACKS,
	G_TASK_FINISHED,
	G_TASK_CANCELLED
} GTaskState;

typedef struct _GTask        GTask;
typedef struct _GTaskPrivate GTaskPrivate;
typedef struct _GTaskClass   GTaskClass;

/**
 * GTaskFunc:
 * @task: the executing #GTask
 * @result: a value to contain the tasks result, if any
 * @user_data: the user data provided to g_task_new
 *
 * This delegate is used to perform the work for a particular task.
 *
 * If the task has result, you can set it after initializing @result with
 * g_value_init().
 *
 * Return value: A #GTask to yield another work item, or NULL
 */
typedef GTask* (*GTaskFunc) (GTask    *task,
                             GValue   *result,
                             gpointer  user_data);

/**
 * GTaskCallback:
 * @task: the executing #GTask
 * @result: the result of the task
 * @user_data: the user data provided to g_task_add_callback()
 *
 * This delegate is used to perform a single callback.
 *
 * If the callback intents to change the current result of the task, it must
 * first unset @result with g_value_unset() followed by g_value_init().
 *
 * Return value: A #GTask to yield another work item, or NULL
 */
typedef GTask* (*GTaskCallback) (GTask    *task,
                                 GValue   *result,
                                 gpointer  user_data);

/**
 * GTaskErrback:
 * @task: the executing #GTask
 * @error: the current error for @task
 * @result: the result for the task
 * @user_data: the user data provided to g_task_add_errback()
 *
 * This delegate is used to perform a single errback.
 *
 * If the task errback has result, you can set it after initializing
 * @result with g_value_init().
 *
 * Return value: A #GTask to yield another work item, or NULL
 */
typedef GTask* (*GTaskErrback) (GTask        *task,
                                const GError *error,
                                GValue       *result,
                                gpointer      user_data);

struct _GTask
{
	/*< private >*/
	GObject parent;

	GTaskPrivate *priv;
};

struct _GTaskClass
{
	/*< private >*/
	GObjectClass parent_class;

	void   (*cancel)    (GTask *task);
	GTask* (*execute)   (GTask *task);

	void   (*reserved1) (void);
	void   (*reserved2) (void);
	void   (*reserved3) (void);
	void   (*reserved4) (void);
	void   (*reserved5) (void);
	void   (*reserved6) (void);
	void   (*reserved7) (void);
	void   (*reserved8) (void);
};

GType         g_task_state_get_type       (void);
GType         g_task_get_type             (void);

GTask*        g_task_new                  (GTaskFunc       func,
                                           gpointer        func_data,
                                           GDestroyNotify  notify);
GTask*        g_task_new_from_closure     (GClosure       *closure);

gboolean      g_task_get_async            (GTask          *task);
void          g_task_set_async            (GTask          *task,
                                           gboolean        is_async);

GTask*        g_task_execute              (GTask          *task);
GTask*        g_task_callback             (GTask          *task);
void          g_task_cancel               (GTask          *task);

gboolean      g_task_has_error            (GTask          *task);
const GError* g_task_get_error            (GTask          *task);
void          g_task_set_error            (GTask          *task,
                                           const GError   *error);

void          g_task_take_error           (GTask          *task,
                                           GError         *error);

const GValue* g_task_get_result           (GTask          *task);
void          g_task_set_result           (GTask          *task,
                                           const GValue   *result);

GTaskState    g_task_get_state            (GTask          *task);
void          g_task_set_state            (GTask          *task,
                                           GTaskState      state);

void          g_task_add_dependency       (GTask          *task,
                                           GTask          *dependency);
void          g_task_remove_dependency    (GTask          *task,
                                           GTask          *dependency);
void          g_task_clear_dependencies   (GTask          *task);

void          g_task_add_callback         (GTask          *task,
                                           GTaskCallback   callback,
                                           gpointer        user_data,
                                           GDestroyNotify  notify);
void          g_task_add_errback          (GTask          *task,
                                           GTaskErrback    errback,
                                           gpointer        user_data,
                                           GDestroyNotify  notify);
void          g_task_add_both             (GTask          *task,
                                           GTaskCallback   callback,
                                           GTaskErrback    errback,
                                           gpointer        user_data,
                                           GDestroyNotify  notify);

void          g_task_add_callback_closure (GTask          *task,
                                           GClosure       *closure);
void          g_task_add_errback_closure  (GTask          *task,
                                           GClosure       *closure);
void          g_task_add_both_closure     (GTask          *task,
                                           GClosure       *callback,
                                           GClosure       *errback);

GTask*        g_task_all_of               (GTask          *first_task, ...);
GTask*        g_task_any_of               (GTask          *first_task, ...);

G_END_DECLS

#endif /* __G_TASK__H__ */

#ifndef __G_TASK_SCHEDULER_H__
#include "gtaskscheduler.h"
#endif
