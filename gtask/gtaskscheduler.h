/* gtaskscheduler.h
 *
 * Copyright (C) 2008 Christian Hergert <chris@dronelabs.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 
 * 02110-1301 USA
 */

#ifndef __G_TASK_SCHEDULER_H__
#define __G_TASK_SCHEDULER_H__

#include <glib-object.h>
#include "gtask.h"

G_BEGIN_DECLS

#define G_TYPE_TASK_SCHEDULER (g_task_scheduler_get_type())

#define G_TASK_SCHEDULER(obj) (             \
    G_TYPE_CHECK_INSTANCE_CAST ((obj),      \
    G_TYPE_TASK_SCHEDULER,                  \
    GTaskScheduler))

#define G_TASK_SCHEDULER_CLASS(klass) (     \
    G_TYPE_CHECK_CLASS_CAST ((klass),       \
    G_TYPE_TASK_SCHEDULER,                  \
    GTaskSchedulerClass))

#define G_IS_TASK_SCHEDULER(obj) (          \
    G_TYPE_CHECK_INSTANCE_TYPE ((obj),      \
    G_TYPE_TASK_SCHEDULER))

#define G_IS_TASK_SCHEDULER_CLASS(klass) (  \
    G_TYPE_CHECK_CLASS_TYPE ((klass),       \
    G_TYPE_TASK_SCHEDULER))

#define G_TASK_SCHEDULER_GET_CLASS(obj) (   \
    G_TYPE_INSTANCE_GET_CLASS ((obj),       \
    G_TYPE_TASK_SCHEDULER,                  \
    GTaskSchedulerClass))

typedef struct _GTaskScheduler        GTaskScheduler;
typedef struct _GTaskSchedulerClass   GTaskSchedulerClass;
typedef struct _GTaskSchedulerPrivate GTaskSchedulerPrivate;

struct _GTaskScheduler
{
	/*< private >*/
	GObject parent;

	GTaskSchedulerPrivate *priv;
};

struct _GTaskSchedulerClass
{
	/*< private >*/
	GObjectClass parent_class;

	void (*callback)   (GTaskScheduler *scheduler,
	                    GTask          *task);

	void (*init)       (GTaskScheduler *scheduler);

	void (*schedule)   (GTaskScheduler *scheduler,
	                    GTask          *task);

	void (*_reserved1) (void);
	void (*_reserved2) (void);
	void (*_reserved3) (void);
	void (*_reserved4) (void);
	void (*_reserved5) (void);
	void (*_reserved6) (void);
	void (*_reserved7) (void);
	void (*_reserved8) (void);
};

GType           g_task_scheduler_get_type    (void);
GTaskScheduler* g_task_scheduler_get_default (void);
GTaskScheduler* g_task_scheduler_new         (void);
void            g_task_scheduler_schedule    (GTaskScheduler *scheduler,
                                              GTask          *task);

G_END_DECLS

#endif /* __G_TASK_SCHEDULER_H__ */
