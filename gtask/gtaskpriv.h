/* gtaskpriv.h
 *
 * Copyright (C) 2008 Christian Hergert <chris@dronelabs.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 
 * 02110-1301 USA
 */

#ifndef __G_TASK_PRIV_H__
#define __G_TASK_PRIV_H__

#include "gtask.h"
#include "gtaskscheduler.h"

G_BEGIN_DECLS

#define G_TASK_ERROR (g_task_error_quark())

typedef struct _GTaskHandler GTaskHandler;

struct _GTaskPrivate
{
	GClosure       *closure;
	GList          *handlers;
	GList          *dependencies;
	GMutex         *mutex;
	GError         *error;

	GTask          *previous;
	GValue          result;

	GTaskScheduler *scheduler;

	GTaskState      state;
	gboolean        is_async;
};

struct _GTaskHandler
{
	GClosure *callback;
	GClosure *errback;
};

typedef enum
{
	G_TASK_ERROR_EXEC,
} GTaskError;


GQuark        g_task_error_quark        (void);
GTaskHandler* g_task_move_next_callback (GTask      *task);
GTaskHandler* g_task_move_next_errback  (GTask      *task);
gboolean      g_task_state_exchange     (GTask      *task,
                                         GTaskState  oldval,
                                         GTaskState  newval);

G_END_DECLS

#endif /* __G_TASK_PRIV_H__ */
