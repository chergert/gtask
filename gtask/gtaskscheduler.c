/* gtaskscheduler.c
 *
 * Copyright (C) 2008 Christian Hergert <chris@dronelabs.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 
 * 02110-1301 USA
 */

/**
 * SECTION:gtaskscheduler
 * @short_description: Scheduling of tasks
 *
 * #GTaskScheduler provides management over how tasks are executed.
 * It can be inherited and tuned for particular problem sets.
 *
 * However, the API as it is now will be going through a series of
 * changes in the time ahead.  So keep that in mind.
 *
 * The base #GTaskScheduler implementation uses a #GThreadPool
 * internally to execute the tasks.  Thread management may well
 * be pulled out of the scheduler soon so that it may concentrate
 * on more important things.
 */

#ifdef _LINUX
#include <sys/sysinfo.h>
#endif

#include "gtaskscheduler.h"
#include "gtaskschedulerpriv.h"
#include "gtaskpriv.h"

#define THREADS_DEFAULT 20

G_DEFINE_TYPE (GTaskScheduler, g_task_scheduler, G_TYPE_OBJECT);

enum {
	PROP_0,
	PROP_MAIN_DISPATCH
};

/**
 * g_task_scheduler_new:
 *
 * Creates a new instance of #GTaskScheduler.  This default scheduler
 * uses a #GThreadPool for task execution.
 *
 * Returns: A new #GTaskScheduler.
 */
GTaskScheduler*
g_task_scheduler_new (void)
{
	return g_object_new (G_TYPE_TASK_SCHEDULER, NULL);
}

/**
 * g_task_scheduler_schedule:
 * @scheduler: A #GTaskScheduler
 * @task: The #GTask to schedule
 * @notify: A #GDestroyNotify to be called after execution of the task
 *          has completed.
 *
 * This method will schedule a task to be executed on a thread of
 * execution.  The default #GTaskScheduler implementation uses a
 * #GThreadPool to perform the task.  g_task_execute() will be called
 * from within the executing thread.
 */
void
g_task_scheduler_schedule (GTaskScheduler *scheduler,
                           GTask          *task)
{
	if (G_TASK_SCHEDULER_GET_CLASS (scheduler)->schedule)
		G_TASK_SCHEDULER_GET_CLASS (scheduler)->schedule (scheduler, task);
}

G_LOCK_DEFINE (default_scheduler);

#ifdef __GNUC__
#pragma GCC diagnostic warning "-Wstrict-aliasing"
/**
 * g_task_scheduler_get_default:
 *
 * Gets the default #GTaskScheduler for the process.
 *
 * Returns: A #GTaskScheduler
 */
GTaskScheduler*
g_task_scheduler_get_default (void)
{
	static GTaskScheduler* default_scheduler = NULL;

	if (!default_scheduler)
	{
		G_LOCK (default_scheduler);
		if (!default_scheduler)
			default_scheduler = g_task_scheduler_new ();
		G_UNLOCK (default_scheduler);
	}

	return default_scheduler;
}
#endif

/**
 * g_task_scheduler_do_cancel_error:
 * @scheduler: A #GTaskScheduler
 * @task: A task whose child has been cancelled
 *
 * This method will set the child cancel error for a task.  Since the
 * portion of the virtual stack after the task has been cancelled, the
 * tasks callback chain will be invoked.  It is up to the callback
 * chain to handle the error.
 */
static void
g_task_scheduler_do_cancel_error (GTaskScheduler *scheduler,
                                  GTask          *task)
{
	GError *error;

	/* Perhaps this should be performed from inside the GTask
	 * rather than here in the scheduler.
	 */

	g_return_if_fail (G_IS_TASK_SCHEDULER (scheduler));
	g_return_if_fail (G_IS_TASK (task));

	/* TODO: Create Error Domains */
	error = g_error_new_literal (0, 0,
	                             "A child task has been cancelled");
	g_task_take_error (task, error);

	g_task_scheduler_callback (scheduler, task);
}

/**
 * g_task_scheduler_callback:
 * @scheduler: A #GTaskScheduler
 * @task: A completed #GTask
 *
 * This method will perform the callbacks for a completed #GTask.
 * If a new task is yielded from a callback, the callback chain will
 * be paused and the new #GTask scheduled for completion.  Upon completion
 * of the new task, the callback chain will be re-initialized.
 *
 * This method is helpful to #GTaskScheduler subclasses to handle
 * the callback work after processing of a task.  It manages the
 * re-entrancy of the callbacks after further tasks complete.
 */
void
g_task_scheduler_callback (GTaskScheduler *scheduler,
                           GTask          *task)
{
	g_return_if_fail (G_IS_TASK_SCHEDULER (scheduler));
	g_return_if_fail (G_IS_TASK (task));

	if (G_TASK_SCHEDULER_GET_CLASS (scheduler)->callback)
		G_TASK_SCHEDULER_GET_CLASS (scheduler)->callback (scheduler, task);
}

/**
 * g_task_scheduler_callback_real_finish:
 * @task: A #GTask
 *
 * This method is called after a task has finished its worker delgate.
 * The callback itscheduler is still in the executing state, however it
 * has not yet finished the callback handling phase.
 */
static gboolean
g_task_scheduler_callback_real_finish (GTask *task)
{
	GTaskScheduler *scheduler;
	GTaskState      state;
	GTask          *result;
	GTask          *previous;
	GError         *error;

	g_return_val_if_fail (G_IS_TASK (task), FALSE);

	scheduler = task->priv->scheduler;
	g_assert (scheduler != NULL);

	state = g_task_get_state (task);
	g_assert (state == G_TASK_FINISHED  ||
	          state == G_TASK_CANCELLED ||
	          state == G_TASK_CALLBACKS);

	result = g_task_callback (task);

	if (result != NULL)
	{
		result->priv->previous = g_object_ref (task);
		g_task_scheduler_schedule (scheduler, result);

		/* NOTE: We have scheduled a new task for processing that was yielded
		 *       during the callback chain.  We defer further processing until
		 *       that GTask has completed. That further processing will happen
		 *       when the new task completes execution and walks up the
		 *       virtual stack.
		 */
		return FALSE;
	}

	if (task->priv->previous)
	{
		previous = task->priv->previous;
		g_object_unref (task);

		/* NOTE: We do tail recursion here as we walk up the virtual
		 *       stack performing callbacks.
		 */
		return g_task_scheduler_callback_real_finish (previous);
	}
	else if ((error = task->priv->error) != NULL)
	{
		/* NOTE: There are no more tasks in the virtual stack and we
		 *       have an unhandled error. Might as well write it to
		 *       the console so it is not lost or hidden.
		 */
		g_warning ("Unhandled error: %s", error->message);
	}

	g_object_unref (task);

	return FALSE;
}

static void
g_task_scheduler_callback_real (GTaskScheduler *scheduler,
                                GTask          *task)
{
	g_return_if_fail (G_IS_TASK_SCHEDULER (scheduler));
	g_return_if_fail (G_IS_TASK (task));

	if (scheduler->priv->main_dispatch)
	{
		/* NOTE: We shouldn't incur the wrath of the powertop gods here
		 *       for not using add_seconds because the CPU is already woken
		 *       up at this very moment, and will continue to be up until
		 *       it finishes the processing of the handler.
		 */
		g_timeout_add (0, (GSourceFunc) g_task_scheduler_callback_real_finish, task);
	}
	else g_task_scheduler_callback_real_finish (task);
}

/**
 * g_task_scheduler_on_state_changed:
 * @task: the #GTask with the changed state
 * @state: the new state
 * @scheduler: A #GTaskScheduler
 *
 * Handles a signal callback for state change on the #GTask.  This method
 * handles the task state change and will schedule the task if its ready.
 * If the state has moved to cancelled, we will notify the parent task.
 */
static void
g_task_scheduler_on_state_changed (GTask          *task,
                                   GTaskState      state,
                                   GTaskScheduler *scheduler)
{
	g_return_if_fail (G_IS_TASK (task));

	switch (state) {
	case G_TASK_READY:
		g_task_scheduler_schedule (scheduler, task);
		break;
	case G_TASK_CANCELLED:
		g_task_scheduler_do_cancel_error (scheduler, task->priv->previous);
		break;
	default:
		return;
	}

	g_signal_handlers_disconnect_by_func (task,
	                                      g_task_scheduler_on_state_changed,
	                                      scheduler);
}

/**
 * g_task_scheduler_start_callbacks:
 * @task: A #GTask
 * @state: the new state of the task
 * @scheduler: A #GTaskScheduler who owns @task
 *
 * This method is used to initiate the start of the callbacks after
 * a task has completed.  By using a signal callback, we use the same
 * code path for callback execution for both synchronous and asynchronous
 * tasks.
 */
static void
g_task_scheduler_start_callbacks (GTask          *task,
                                  GTaskState      state,
                                  GTaskScheduler *scheduler)
{
	if (state != G_TASK_CALLBACKS && state != G_TASK_CANCELLED)
		return;

	g_signal_handlers_disconnect_by_func (task,
	                                      g_task_scheduler_start_callbacks,
	                                      scheduler);

	g_task_scheduler_callback (scheduler, task);
}

/**
 * g_task_scheduler_thread_pool_func:
 * @data: a pointer to the worker data
 * @user_data: a pointer to a #GTaskScheduler
 *
 * This method manages the work inside of a thread.  It begins the
 * worker for the task and starts the invocation of the callback
 * chain if needed.
 *
 * If a task results in another task, it will be immediately executed
 * if it is in the G_TASK_READY state.  Otherwise, It will be monitored
 * and executed when that state is reached.
 */
static void
g_task_scheduler_thread_pool_func (gpointer data,
                                   gpointer user_data)
{
	GTaskScheduler *scheduler;
	GTask          *task,
	               *result,
	               *current;

	g_return_if_fail (G_IS_TASK_SCHEDULER (user_data));
	g_return_if_fail (G_IS_TASK (data));

	scheduler = G_TASK_SCHEDULER (user_data);
	task = G_TASK (data);

	/* check that the task is ready, and change to executing */
	if (!g_task_state_exchange (task, G_TASK_READY, G_TASK_EXECUTING))
		return;

	/* When the task finishes, we can schedule it for callbacks.
	 * Using the signal, we can have the same code path for both
	 * synchronous and asynchronous tasks.
	 */
	g_signal_connect (task, "state-changed",
	                  G_CALLBACK (g_task_scheduler_start_callbacks),
	                  scheduler);

	current = task;

	while (NULL != (result = g_task_execute (current)))
	{
		/* NOTE: If we were cancelled while executing, we will set the
		 *       error and return right away.
		 */
		if (g_task_get_state (current) == G_TASK_CANCELLED)
		{
			g_task_scheduler_do_cancel_error (scheduler, current);
			return;
		}

		result->priv->previous = current;
		current = result;

		if (g_task_get_state (result) == G_TASK_CANCELLED)
		{
			/* NOTE: If the yielded task is already cancelled, we create
			 *       a new error and attach it to the parent task.  It is
			 *       then up to the parent task to handle the error in its
			 *       errback chain.  We also start the process of that
			 *       parents callback chain.
			 */
			if (result->priv->previous)
			{
				g_task_scheduler_do_cancel_error (scheduler, result->priv->previous);
				return;
			}
		}
		else if (result->priv->state != G_TASK_READY)
		{
			/* NOTE: We cannot continue processing until this task is ran.
			 *       Lets connect to its state-changed signal and defer
			 *       scheduling the task until the state is G_TASK_READY.
			 *       We are done after connecting the signal as this stack
			 *       is paused.
			 */
			g_mutex_lock (result->priv->mutex);
			if (result->priv->state != G_TASK_READY)
				g_signal_connect (result, "state-changed",
				                  G_CALLBACK (g_task_scheduler_on_state_changed),
				                  scheduler);
			g_mutex_unlock (result->priv->mutex);
			return;
		}
	}

	if (!g_task_get_async (current))
		g_task_set_state (current, G_TASK_FINISHED);
}

/**
 * g_task_scheduler_schedule_real:
 * @scheduler: A #GTaskScheduler
 * @task: A #GTask to schedule
 *
 * This is the defualt scheduler implementation func.  It pushes the
 * work item onto a worker thread in our thread pool.
 */
static void
g_task_scheduler_schedule_real (GTaskScheduler *scheduler, GTask *task)
{
	gboolean is_ready = TRUE;

	g_return_if_fail (G_IS_TASK_SCHEDULER (scheduler));
	g_return_if_fail (G_IS_TASK (task));

	/* NOTE: This method can be called multiple times since it is
	 *       also used to re-schedule a task that has been previously
	 *       scheduled but was not ready for execution.
	 */

	g_mutex_lock (task->priv->mutex);

	if (!task->priv->scheduler)
	{
		task->priv->scheduler = g_object_ref (scheduler);
		g_object_ref (task);

		if (task->priv->state != G_TASK_READY)
		{
			is_ready = FALSE;
			g_signal_connect (task,
			                  "state-changed",
			                  G_CALLBACK (g_task_scheduler_on_state_changed),
			                  scheduler);
		}
	}

	if (is_ready)
		g_thread_pool_push (scheduler->priv->thread_pool, task, NULL);

	g_mutex_unlock (task->priv->mutex);
}

/**
 * g_task_scheduler_init_real:
 * @scheduler: A #GTaskScheduler
 *
 * Default init implementation for GTaskScheduler. Creates the thread pool
 * that is only used by the default implementation.
 */
static void
g_task_scheduler_init_real (GTaskScheduler *scheduler)
{
	gint n_threads = THREADS_DEFAULT;

	g_return_if_fail (G_IS_TASK_SCHEDULER (scheduler));

#ifdef _LINUX
	n_threads = get_nprocs () * 10;
#endif

	scheduler->priv->thread_pool =
		g_thread_pool_new (g_task_scheduler_thread_pool_func,
		                   scheduler, n_threads, FALSE,
		                   NULL);
}

static void
g_task_scheduler_get_property (GObject    *object,
                               guint       property_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
	g_return_if_fail (G_IS_TASK_SCHEDULER (object));

	switch (property_id) {
	case PROP_MAIN_DISPATCH:
		g_value_set_boolean (value, G_TASK_SCHEDULER (object)->priv->main_dispatch);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
	}
}

static void
g_task_scheduler_set_property (GObject      *object,
                               guint         property_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
	g_return_if_fail (G_IS_TASK_SCHEDULER (object));

	switch (property_id) {
	case PROP_MAIN_DISPATCH:
		G_TASK_SCHEDULER (object)->priv->main_dispatch = g_value_get_boolean (value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
	}
}

static void
g_task_scheduler_dispose (GObject *object)
{
	if (G_OBJECT_CLASS (g_task_scheduler_parent_class)->dispose)
		G_OBJECT_CLASS (g_task_scheduler_parent_class)->dispose (object);
}

static void
g_task_scheduler_finalize (GObject *object)
{
	G_OBJECT_CLASS (g_task_scheduler_parent_class)->finalize (object);
}

static void
g_task_scheduler_class_init (GTaskSchedulerClass *task_sched_class)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (task_sched_class);

	g_type_class_add_private (task_sched_class, sizeof (GTaskSchedulerPrivate));

	task_sched_class->callback  = g_task_scheduler_callback_real;
	task_sched_class->init      = g_task_scheduler_init_real;
	task_sched_class->schedule  = g_task_scheduler_schedule_real;

	gobject_class->get_property = g_task_scheduler_get_property;
	gobject_class->set_property = g_task_scheduler_set_property;
	gobject_class->dispose      = g_task_scheduler_dispose;
	gobject_class->finalize     = g_task_scheduler_finalize;

	/**
	 * GTaskScheduler:main-dispatch:
	 *
	 * The main-dispatch property determines if tasks processed by the
	 * scheduler should perform callbacks inside of the main thread.
	 *
	 * The default value is TRUE.
	 *
	 * By performing callbacks in the main thread, gtk+ developers can
	 * avoid the task of acquiring the GDK thread lock as their dispatching
	 * thread will already own it.
	 */
	g_object_class_install_property (gobject_class,
	                                 PROP_MAIN_DISPATCH,
	                                 g_param_spec_boolean ("main-dispatch",
	                                                       "Main Dispatch",
	                                                       "Dispatch via main thread",
	                                                       TRUE,
	                                                       G_PARAM_READWRITE));
}

static void
g_task_scheduler_init (GTaskScheduler *scheduler)
{
	scheduler->priv = G_TYPE_INSTANCE_GET_PRIVATE (scheduler,
	                                               G_TYPE_TASK_SCHEDULER,
	                                               GTaskSchedulerPrivate);
	scheduler->priv->main_dispatch = TRUE;

	if (G_TASK_SCHEDULER_GET_CLASS (scheduler)->init)
		G_TASK_SCHEDULER_GET_CLASS (scheduler)->init (scheduler);
}
