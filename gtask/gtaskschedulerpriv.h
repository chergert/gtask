/* gtaskschedulerpriv.h
 *
 * Copyright (C) 2008 Christian Hergert <chris@dronelabs.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 
 * 02110-1301 USA
 */

#ifndef __G_TASK_SCHEDULER_PRIV_H__
#define __G_TASK_SCHEDULER_PRIV_H__

#include <glib.h>

G_BEGIN_DECLS

typedef struct _GTaskSchedulerArgs GTaskSchedulerArgs;

struct _GTaskSchedulerPrivate
{
	GThreadPool *thread_pool;
	gboolean     main_dispatch;
};

void g_task_scheduler_callback (GTaskScheduler *scheduler,
                                GTask          *task);

G_END_DECLS

#endif /* __G_TASK_SCHEDULERPRIV_H__ */
