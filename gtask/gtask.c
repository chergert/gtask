/* gtask.c
 *
 * Copyright (C) 2008 Christian Hergert <chris@dronelabs.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 
 * 02110-1301 USA
 */

/**
 * SECTION:gtask
 * @short_description: Asynchronous and callback-based execution
 *
 * #GTask is a #GObject sublcass that encapsulates a work item.  Tasks
 * remove the concept of a standard stack, but more on that in a minute.
 *
 * The power of #GTask comes from the callback and errback handling
 * strategy.  You can, ahead of time, create complex callback chains
 * for handling normal and extraordinary flow within your application.
 *
 * Callbacks are run sequentially after task execution has completed. You
 * may also have errbacks, which are like a callback but used to handle
 * an error in task execution or previous callback.
 *
 * If a callback creates an error, no more callbacks will be executed
 * until we reach another errback.
 * 
 * This documentation sucks, I know it.  I'll come back later and improve
 * it.  Read the section on twisted deferreds for now.
 */

#include "gtask.h"
#include "gtaskpriv.h"
#include "gtaskmarshal.h"

G_DEFINE_TYPE (GTask, g_task, G_TYPE_OBJECT);

enum {
	STATE_CHANGED,
	LAST_SIGNAL
};

enum {
	PROP_0,
	PROP_STATE
};

static gulong signals[LAST_SIGNAL] = { 0, };

GQuark
g_task_error_quark (void)
{
	return g_quark_from_static_string ("g_task_error-quark");
}

/**
 * g_task_new:
 * @func: A #GTaskFunc to be executed during g_task_execute()
 * @func_data: The user data for @func
 * @notify: The destroy notification for @func_data
 *
 * Creates a new instance of #GTask and sets the #GTaskFunc to use when
 * g_task_execute() is called.  @func will be called from a thread.
 *
 * Return value: A new instance of #GTask.
 */
GTask*
g_task_new (GTaskFunc func, gpointer func_data, GDestroyNotify notify)
{
	GClosure *closure;

	closure = g_cclosure_new (G_CALLBACK (func),
	                          func_data,
	                          (GClosureNotify)notify);

	g_closure_set_marshal (closure, _gtask_marshal_OBJECT__POINTER);

	return g_task_new_from_closure (closure);
}

/**
 * g_task_new_from_closure:
 * @closure: A #GClosure
 *
 * Creates a new instance of #GTask and sets up the task to invoke
 * @closure when the task is executed. The closure will be called
 * from a thread.
 *
 * Return value: A new instance of #GTask.
 */
GTask*
g_task_new_from_closure (GClosure *closure)
{
	GTask *task;

	task = g_object_new (G_TYPE_TASK, NULL);

	if (closure)
	{
		task->priv->closure = g_closure_ref (closure);
		g_object_watch_closure (G_OBJECT (task), closure);
		g_closure_sink (closure);
	}

	return task;
}

/**
 * g_task_execute:
 * @task: A #GTask
 * @error: A location to return an error
 * @see_also: g_task_callback()
 *
 * Synchronously performs the task's worker delegate.  Tasks that have more
 * work to complete will return a new #GTask.  If they have completed then
 * NULL is returned.
 *
 * Scheduler implementors are highly encouraged to immediately execute
 * the potentially returned task to reduce scheduling overhead.
 *
 * It is also the #GTaskScheduler<!-- -->s responsibility to invoke the callback
 * chain after execution via g_task_callback().
 *
 * Return value: a new #GTask to perform if additional work is required or NULL.
 */
GTask*
g_task_execute (GTask *task)
{
	GTask *result = NULL;

	g_return_val_if_fail (G_IS_TASK (task), NULL);

	if (G_TASK_GET_CLASS (task)->execute)
		result = G_TASK_GET_CLASS (task)->execute (task);

	if (!task->priv->is_async)
		g_task_set_state (task, G_TASK_CALLBACKS);

	return result;
}

/**
 * g_task_set_result:
 * @task: A #GTask
 * @value: a #GValue which will be copied
 *
 * Sets the current result for the task.  The value is copied and stored
 * to be passed as a parameter to the next callback.
 */
void
g_task_set_result (GTask *task, const GValue *value)
{
	g_return_if_fail (G_IS_TASK (task));

	g_mutex_lock (task->priv->mutex);

	if (G_VALUE_TYPE (&task->priv->result) != 0)
		g_value_unset (&task->priv->result);

	if (value && G_VALUE_TYPE (value))
	{
		g_value_init (&task->priv->result, G_VALUE_TYPE (value));
		g_value_copy (value, &task->priv->result);
	}

	g_mutex_unlock (task->priv->mutex);
}

/**
 * g_task_get_result:
 * @task: A #GTask
 *
 * Retrieves the current value for the task.  The value should not be
 * modified or freed.
 *
 * Return value: The current result.
 */
const GValue*
g_task_get_result (GTask *task)
{
	g_return_val_if_fail (G_IS_TASK (task), NULL);
	return &task->priv->result;
}

/**
 * g_task_cancel:
 * @task: A #GTask
 *
 * Cancels a task.  If the task has not started, it will not be executed
 * and tasks depending on it will be notified.  If the task is being
 * executed, it is up to the executing worker delegate to check for the
 * cancellation request and terminate execution.
 *
 * Callbacks and errbacks may still perform if they handle the error that
 * will be created for the task.
 */
void
g_task_cancel (GTask *task)
{
	g_return_if_fail (G_IS_TASK (task));

	/* You cannot cancel a task after it has already completed */
	if (task->priv->state != G_TASK_WAITING &&
	    task->priv->state != G_TASK_READY   &&
	    task->priv->state != G_TASK_EXECUTING)
		return;

	g_task_set_state (task, G_TASK_CANCELLED);
}

/**
 * g_task_get_state:
 * @task: A #GTask
 *
 * Retrieves the current state of a task.
 *
 * Return value: a #GTaskState representing the current state.
 */
GTaskState
g_task_get_state (GTask *task)
{
	g_return_val_if_fail (G_IS_TASK (task), -1);
	return task->priv->state;
}

/**
 * g_task_get_async:
 * @task: A #GTask
 *
 * Whether or not this task is asynchronous.
 *
 * Return value: TRUE if the task is asynchronous
 */
gboolean
g_task_get_async (GTask *task)
{
	g_return_val_if_fail (G_IS_TASK (task), FALSE);
	return task->priv->is_async;
}

/**
 * g_task_set_async:
 * @task: A #GTask
 * @is_async: if the task is asynchronous
 *
 * Asynchronous tasks are tasks whose execution closure will return
 * immediately but not be finished processing until later.
 *
 * Note that asynchronous tasks need to update their state to
 * @G_TASK_FINISHED using g_task_set_state().
 */
void
g_task_set_async (GTask *task, gboolean is_async)
{
	g_return_if_fail (G_IS_TASK (task));

	g_mutex_lock (task->priv->mutex);

	/* you can't change async state after its started executing */
	if (task->priv->state == G_TASK_READY || task->priv->state == G_TASK_WAITING)
		task->priv->is_async = is_async;

	g_mutex_unlock (task->priv->mutex);
}

/*
 * This method is attached to a dependent tasks state-changed
 * signal.  It is used to dispatch tasks when dependent tasks
 * have finished.
 */
static void
_g_task_on_state_changed (GTask      *dependency,
                          GTaskState  state,
                          gpointer    user_data)
{
	GTask *task;

	g_return_if_fail (G_IS_TASK (dependency));
	g_return_if_fail (G_IS_TASK (user_data));

	task = G_TASK (user_data);

	switch (state) {
	case G_TASK_FINISHED:
		g_task_remove_dependency (task, dependency);
		break;
	case G_TASK_CANCELLED:
		g_task_cancel (task);
		break;
	case G_TASK_CALLBACKS:
	case G_TASK_WAITING:
	case G_TASK_READY:
	case G_TASK_EXECUTING:
	default:
		break;
	}
}

/**
 * g_task_add_dependency:
 * @task: A #GTask
 * @dependency: A #GTask which must complete before execution
 *
 * Adds a #GTask to the list of tasks that must be completed before this
 * task may execute.
 */
void
g_task_add_dependency (GTask *task, GTask *dependency)
{
	g_return_if_fail (G_IS_TASK (task));
	g_return_if_fail (G_IS_TASK (dependency));

	if (task->priv->state > G_TASK_READY)
		return;
	else if (g_task_get_state (dependency) == G_TASK_FINISHED)
		return;

	g_mutex_lock (task->priv->mutex);

	task->priv->dependencies = g_list_prepend (task->priv->dependencies,
	                                           g_object_ref (dependency));
	task->priv->state = G_TASK_WAITING;

	g_mutex_unlock (task->priv->mutex);

	g_signal_connect (dependency, "state-changed",
	                  G_CALLBACK (_g_task_on_state_changed),
	                  task);
}

/**
 * g_task_clear_dependencies:
 * @task: A #GTask
 *
 * Removes all dependencies for a given task. The default scheduler will
 * execute the task if it has previously been scheduled and was just
 * waiting on dependent tasks to complete.
 */
void
g_task_clear_dependencies (GTask *task)
{
	GList    *iter;
	gboolean  do_state = FALSE;

	g_return_if_fail (G_IS_TASK (task));

	g_mutex_lock (task->priv->mutex);

	if (!task->priv->dependencies)
		goto cleanup;

	do_state = TRUE;

	for (iter = task->priv->dependencies; iter; iter = iter->next)
	{
		g_signal_handlers_disconnect_by_func (iter->data,
		                                      _g_task_on_state_changed,
		                                      task);
		g_object_unref (iter->data);
	}

	task->priv->dependencies = NULL;

cleanup:
	g_mutex_unlock (task->priv->mutex);

	if (do_state)
		g_task_set_state (task, G_TASK_READY);
}

/**
 * g_task_remove_dependency:
 * @task: A #GTask
 * @dependency: A #GTask that is currently a dependency of @task
 *
 * Removes a currently dependent task. If all dependencies have been
 * met, the tasks state will move to @G_TASK_READY.
 *
 * If the task has previously been scheduled then the task will be
 * re-submitted to the scheduler for execution.
 */
void
g_task_remove_dependency (GTask *task,
                          GTask *dependency)
{
	GList    *iter;
	gboolean  do_state = FALSE;

	g_return_if_fail (G_IS_TASK (task));
	g_return_if_fail (G_IS_TASK (dependency));

	g_mutex_lock (task->priv->mutex);

	for (iter = task->priv->dependencies; iter; iter = iter->next)
	{
		if (iter->data == dependency)
		{
			task->priv->dependencies = g_list_delete_link (task->priv->dependencies, iter);
			g_signal_handlers_disconnect_by_func (dependency,
			                                      _g_task_on_state_changed,
			                                      task);
			g_object_unref (dependency);
			do_state = task->priv->dependencies == NULL &&
			           task->priv->state == G_TASK_WAITING;
			break;
		}
	}

	g_mutex_unlock (task->priv->mutex);

	if (do_state)
		g_task_set_state (task, G_TASK_READY);
}

/*
 * Adds a task handler to the callback/errback execution chain.  This is used
 * to DRY the addition process from errback and callback addition methods.
 *
 * It expects the mutex to already by locked.
 */
static void
_g_task_add_handler (GTask    *task,
                     GClosure *callback,
                     GClosure *errback)
{
	GTaskHandler *handler;

	g_return_if_fail (G_IS_TASK (task));

	handler = g_slice_new0 (GTaskHandler);

	g_mutex_lock (task->priv->mutex);

	if (callback)
	{
		handler->callback = g_closure_ref (callback);
		g_object_watch_closure (G_OBJECT (task), callback);
		g_closure_sink (callback);
	}

	if (errback)
	{
		handler->errback = g_closure_ref (errback);
		g_object_watch_closure (G_OBJECT (task), errback);
		g_closure_sink (errback);
	}

	task->priv->handlers = g_list_append (task->priv->handlers, handler);

	g_mutex_unlock (task->priv->mutex);
}

/**
 * g_task_add_callback:
 * @task: A #GTask
 * @callback: A #GTaskFunc
 * @user_data: user data for @callback
 * @notify: destroy notification for @user_data
 *
 * Adds a callback handler to the processing chain.  This is executed
 * in turn during the callback phase.  If the callback generates an error,
 * the first error handler left in the chain will run.  If the callback does
 * not generate an error, further callbacks may be called.
 */
void
g_task_add_callback (GTask          *task,
                     GTaskCallback   callback,
                     gpointer        user_data,
                     GDestroyNotify  notify)
{
	GClosure *closure;

	closure = g_cclosure_new (G_CALLBACK (callback),
	                          user_data,
	                          (GClosureNotify)notify);

	g_closure_set_marshal (closure, _gtask_marshal_OBJECT__POINTER);

	_g_task_add_handler (task, closure, NULL);
}

/**
 * g_task_add_callback_closure:
 * @task: A #GTask
 * @closure: A #GClosure to execute
 *
 * This method is similar to g_task_add_callback() except it allows
 * the use of a closure.  Binding authors should use this method to
 * allow for greater control and flexibility.
 */
void
g_task_add_callback_closure (GTask *task, GClosure *closure)
{
	_g_task_add_handler (task, closure, NULL);
}

/**
 * g_task_add_errback:
 * @task: A #GTask
 * @errback: A #GTaskFunc
 * @user_data: user data for @errback
 * @notify: destroy notification for @user_data
 *
 * Adds an errback handler to the processing chain.  This is executed in
 * turn during the callbacks phase.  If the errback unsets the error, then
 * callbacks added after the errback may continue to process.  If the
 * errback does not unset the error, further errback handlers may be
 * processed.
 *
 * To unset an error during an errback, use g_task_set_error() with
 * a NULL error.
 */
void
g_task_add_errback (GTask          *task,
                    GTaskErrback    errback,
                    gpointer        user_data,
                    GDestroyNotify  notify)
{
	GClosure *closure;

	closure = g_cclosure_new (G_CALLBACK (errback),
	                          user_data,
	                          (GClosureNotify)notify);

	g_closure_set_marshal (closure, _gtask_marshal_OBJECT__POINTER_POINTER);

	_g_task_add_handler (task, NULL, closure);
}

/**
 * g_task_add_errback_closure:
 * @task: A #GTask
 * @closure: A #GClosure
 *
 * This method is similar to g_task_add_errback() except it allows
 * the use of a closure.  Binding authors should be using this method
 * to allow for greater control and flexibility.
 */
void
g_task_add_errback_closure (GTask *task, GClosure *closure)
{
	_g_task_add_handler (task, NULL, closure);
}

/**
 * g_task_add_both:
 * @task: A #GTask
 * @callback: A #GTaskCallback
 * @errback: A #GTaskErrback
 * @user_data: user data for @callback and @errback
 * @notify: destroy notification for @user_data
 *
 * Adds both an errback and a callback to a handler in the callbacks phase.
 * This means that one of these methods is garunteed to be executed during
 * the callbacks phase.  When the handler is reached, if the task currently
 * has an error, @errback will be called, otherwise, @callback will be called.
 */
void
g_task_add_both (GTask          *task,
                 GTaskCallback   callback,
                 GTaskErrback    errback,
                 gpointer        user_data,
                 GDestroyNotify  notify)
{
	GClosure *callback_closure;
	GClosure *errback_closure;

	callback_closure = g_cclosure_new (G_CALLBACK (callback),
	                                   user_data,
	                                   (GClosureNotify)notify);
	errback_closure = g_cclosure_new (G_CALLBACK (errback),
	                                  user_data,
	                                  (GClosureNotify)notify);

	g_closure_set_marshal (callback_closure, _gtask_marshal_OBJECT__POINTER);
	g_closure_set_marshal (errback_closure, _gtask_marshal_OBJECT__POINTER_POINTER);

	_g_task_add_handler (task, callback_closure, errback_closure);
}

/**
 * g_task_add_both_closure:
 * @task: A #GTask
 * @callback: a #GClosure for the GTaskCallback.
 * @errback: a #GClosure for the GTaskErrback.
 *
 * This method is identical to g_task_add_both() except it allows the use
 * of closures directly.
 *
 * Use this for simple branching control of exceptions.
 */
void
g_task_add_both_closure (GTask    *task,
                         GClosure *callback,
                         GClosure *errback)
{
	_g_task_add_handler (task, callback, errback);
}

/**
 * g_task_set_state:
 * @task: A #GTask
 * @state: The new #GTaskState for @task
 *
 * Updates the current state of a #GTask.  Changing the state will
 * emit the state-changed signal.
 */
void
g_task_set_state (GTask      *task,
                  GTaskState  state)
{
	gboolean do_state = FALSE;

	g_return_if_fail (G_IS_TASK (task));

	g_mutex_lock (task->priv->mutex);

	do_state = task->priv->state != state;
	task->priv->state = state;

	if (state == G_TASK_FINISHED && task->priv->closure)
	{
		g_closure_unref (task->priv->closure);
		task->priv->closure = NULL;
	}

	g_mutex_unlock (task->priv->mutex);

	if (do_state)
		g_signal_emit (task, signals [STATE_CHANGED], 0, state);
}

/*
 * Looses our reference on a callback or errback handler. The memory
 * consumed by handler is freed.
 */
static void
_g_task_handler_free (GTaskHandler *handler)
{
	if (handler->callback)
		g_closure_unref (handler->callback);

	if (handler->errback)
		g_closure_unref (handler->errback);

	g_slice_free (GTaskHandler, handler);
}

/*
 * Executes a closure expecting a closure signature of a #GTaskFunc.
 */
static void
_g_task_func_closure_invoke (GTask    *task,
                             GClosure *closure,
                             GValue   *result)
{
	GValue param_values[2] = {{0}, {0}};

	g_return_if_fail (G_IS_TASK (task));

	/* Parameter 1 in GTaskFunc */
	g_value_init (&param_values[0], G_TYPE_OBJECT);
	g_value_set_object (&param_values[0], task);

	/* Parameter 2 in GTaskFunc */
	g_value_init (&param_values[1], G_TYPE_POINTER);
	g_value_set_pointer (&param_values[1], &task->priv->result);

	g_closure_invoke (closure, result, 2, &param_values[0], NULL);

	g_value_unset (&param_values[0]);
	g_value_unset (&param_values[1]);
}

/*
 * Executes a closure expecting a closure signature of a #GTaskCallback.
 */
static void
_g_task_callback_closure_invoke (GTask    *task,
                                 GClosure *closure,
                                 GValue   *result)
{
	GValue param_values[2] = {{0}, {0}};

	g_return_if_fail (G_IS_TASK (task));

	/* Parameter 1 in GTaskCallback */
	g_value_init (&param_values[0], G_TYPE_OBJECT);
	g_value_set_object (&param_values[0], task);

	/* Parameter 2 in GTaskCallback */
	g_value_init (&param_values[1], G_TYPE_POINTER);
	g_value_set_pointer (&param_values[1], &task->priv->result);

	g_closure_invoke (closure, result, 2, &param_values[0], NULL);

	g_value_unset (&param_values[0]);
	g_value_unset (&param_values[1]);
}

/*
 * Executes a closure expecting a closure signature of a #GTaskErrback.
 */
static void
_g_task_errback_closure_invoke (GTask    *task,
                                GClosure *closure,
                                GValue   *result)
{
	GValue param_values[3] = {{0}, {0}, {0}};

	g_return_if_fail (G_IS_TASK (task));

	/* Parameter 1 in GTaskErrback */
	g_value_init (&param_values[0], G_TYPE_OBJECT);
	g_value_set_object (&param_values[0], task);

	g_assert (task->priv->error != NULL);

	/* Parameter 2 in GTaskErrback */
	g_value_init (&param_values[1], G_TYPE_POINTER);
	g_value_set_pointer (&param_values[1], task->priv->error);

	/* Parameter 3 in GTaskErrback */
	g_value_init (&param_values[2], G_TYPE_POINTER);
	g_value_set_pointer (&param_values[2], &task->priv->result);

	g_closure_invoke (closure, result, 3, &param_values[0], NULL);

	g_value_unset (&param_values[0]);
	g_value_unset (&param_values[1]);
	g_value_unset (&param_values[2]);
}

/**
 * g_task_callback:
 * @task: A #GTask
 *
 * Works towards completing the execution of the callback chain. If the
 * task contains an error, we will try to resolve the error by jumping
 * to the next errback handler.
 *
 * If a callback or errback return a new #GTask to execute, the callback
 * chain will pause and wait for this new task to be completed.
 *
 * When the resulted task has been completed, the scheduler implementation
 * should recall this method to work towards finishing the execution of
 * the callbacks.
 *
 * Return value: A new #GTask or NULL if the callback chain has completed.
 */
GTask*
g_task_callback (GTask *task)
{
	GTask        *result_task  = NULL;
	GValue        result_value = {0};
	GTaskHandler *handler      = NULL;
	gboolean      has_error;

	g_return_val_if_fail (G_IS_TASK (task), NULL);
	g_return_val_if_fail (task->priv->state == G_TASK_FINISHED  ||
	                      task->priv->state == G_TASK_CALLBACKS ||
	                      task->priv->state == G_TASK_CANCELLED,
	                      NULL);

	if (task->priv->state == G_TASK_FINISHED || task->priv->state == G_TASK_CANCELLED)
		g_task_set_state (task, G_TASK_CALLBACKS);

	/* Execute the callback/errback handlers */
	while (G_LIKELY (task->priv->handlers != NULL))
	{
		has_error = g_task_has_error (task);

		if (has_error)
			handler = g_task_move_next_errback (task);
		else
			handler = g_task_move_next_callback (task);

		if (G_LIKELY (handler))
		{
			g_value_init (&result_value, G_TYPE_OBJECT);

			if (has_error)
				_g_task_errback_closure_invoke (task, handler->errback, &result_value);
			else
				_g_task_callback_closure_invoke (task, handler->callback, &result_value);

			result_task = g_value_dup_object (&result_value);

			g_value_unset (&result_value);
			_g_task_handler_free (handler);

			if (result_task)
				return result_task;
		}
		else break;
	}

	/* Propagate error/result to parent task */
	if (task->priv->previous)
	{
		if (task->priv->error)
			g_task_set_error (task->priv->previous, task->priv->error);
		else
			g_task_set_result (task->priv->previous, &task->priv->result);
	}

	return NULL;
}

/*
 * Retrieves the current handler and increments the list to the next
 * position.  The caller owns the reference and must free it with
 * g_free().
 *
 * Return value: a caller owned #GTaskHandler or NULL.
 */
static GTaskHandler*
_g_task_move_next_unlocked (GTask *task)
{
	GTaskHandler *handler = NULL;

	g_return_val_if_fail (G_IS_TASK (task), NULL);

	if (!task->priv->handlers)
		return NULL;

	handler = task->priv->handlers->data;
	task->priv->handlers = g_list_delete_link (task->priv->handlers,
	                                           task->priv->handlers);

	return handler;
}

/**
 * g_task_move_next_callback:
 * @task: A #GTask
 *
 * This method will move to the next handler which is a callback
 * task handler.  The newly current #GTaskHandler will be returned.
 *
 * Return value: The next #GTaskHandler that is a callback or NULL.
 */
GTaskHandler*
g_task_move_next_callback (GTask *task)
{
	GTaskHandler *handler = NULL;

	g_return_val_if_fail (G_IS_TASK (task), NULL);

	if (!task->priv->handlers)
		return NULL;

	g_mutex_lock (task->priv->mutex);

	while (NULL != (handler = _g_task_move_next_unlocked (task)))
	{
		if (handler->callback)
			break;
		_g_task_handler_free (handler);
	}

	g_mutex_unlock (task->priv->mutex);

	return handler;
}

/**
 * g_task_move_next_errback:
 * @task: A #GTask
 *
 * This method will move to the next handler that is an errback.
 * The newly current #GTaskHandler will be returned.
 *
 * Return value: The next #GTaskHandler which is an errback or NULL.
 */
GTaskHandler*
g_task_move_next_errback (GTask *task)
{
	GTaskHandler *handler = NULL;

	g_return_val_if_fail (G_IS_TASK (task), NULL);

	if (!task->priv->handlers)
		return NULL;

	g_mutex_lock (task->priv->mutex);

	while (NULL != (handler = _g_task_move_next_unlocked (task)))
	{
		if (handler->errback)
			break;
		_g_task_handler_free (handler);
	}

	g_mutex_unlock (task->priv->mutex);

	return handler;
}

/**
 * _g_task_execute_real:
 * @task: A #GTask
 * @error: a location for an error
 *
 * The default execute implementation for a #GTask.  It handles both
 * synchronous and asynchronous task implementations.
 */
static GTask*
_g_task_execute_real (GTask *task)
{
	GValue result = {0};
	GTask *task_result;

	g_return_val_if_fail (G_IS_TASK (task), NULL);

	g_value_init (&result, G_TYPE_OBJECT);

	_g_task_func_closure_invoke (task, task->priv->closure, &result);

	task_result = g_value_get_object (&result);
	g_value_unset (&result);

	return task_result;
}

/**
 * g_task_has_error:
 * @task: A #GTask.
 *
 * This method checks to see if there is currently an error for the task.
 *
 * Return value: TRUE if there is an error
 */
gboolean
g_task_has_error (GTask *task)
{
	g_return_val_if_fail (G_IS_TASK (task), FALSE);
	return task->priv->error != NULL;
}

/**
 * g_task_get_error:
 * @task: A #GTask.
 *
 * This method will retrieve the current error for the task.  It should
 * not be modified directly.
 *
 * Return value: A #GError that should not be modified.
 */
G_CONST_RETURN GError*
g_task_get_error (GTask *task)
{
	g_return_val_if_fail (G_IS_TASK (task), NULL);
	return task->priv->error;
}

/**
 * g_task_set_error:
 * @task: A #GTask
 * @error: a #GError
 *
 * Sets the current error for the task.  @error is copied locally.
 */
void
g_task_set_error (GTask *task, const GError *error)
{
	g_return_if_fail (G_IS_TASK (task));

	g_mutex_lock (task->priv->mutex);

	if (task->priv->error)
		g_error_free (task->priv->error);

	if (error)
		task->priv->error = g_error_copy (error);
	else
		task->priv->error = NULL;

	g_mutex_unlock (task->priv->mutex);
}

/**
 * g_task_take_error:
 * @task: A #GTask
 * @error: A #GError
 *
 * Like g_task_set_error() but the error is used directly and steals
 * ownership of the error.
 */
void
g_task_take_error (GTask  *task,
                   GError *error)
{
	g_return_if_fail (G_IS_TASK (task));

	g_mutex_lock (task->priv->mutex);

	task->priv->error = error;

	g_mutex_unlock (task->priv->mutex);
}

/**
 * g_task_state_exchange:
 * @task: A #GTask
 * @oldval: the old value 
 * @newval: the new value
 *
 * Checks to see if the tasks state matches @oldval. If it does, the state
 * is changed to @newval.
 *
 * This should be used similar to a cmpxchg, except for it incurs a lock.
 */
gboolean
g_task_state_exchange (GTask      *task,
                       GTaskState  oldval,
                       GTaskState  newval)
{
	gboolean success = FALSE;

	g_return_val_if_fail (G_IS_TASK (task), FALSE);

	g_mutex_lock (task->priv->mutex);

	if (task->priv->state == oldval)
	{
		task->priv->state = newval;
		success = TRUE;
	}

	g_mutex_unlock (task->priv->mutex);

	if (success)
		g_signal_emit (task, signals [STATE_CHANGED], 0, newval);

	return success;
}

static GTask*
_g_task_dummy_func (GTask    *task,
                    GValue   *result,
                    gpointer  user_data)
{
  return NULL;
}

GTask*
g_task_all_of (GTask *first_task, ...)
{
	va_list  args;
	GTask   *task;
	GTask   *iter_task;

	if (first_task == NULL)
		return NULL;

	task = g_task_new (_g_task_dummy_func, NULL, NULL);
	iter_task = first_task;

	va_start (args, first_task);

	while (iter_task != NULL)
	{
		if (G_IS_TASK (iter_task))
			g_task_add_dependency (task, iter_task);
		iter_task = va_arg (args, GTask*);
	}

	va_end (args);

	return task;
}

static GTask*
_g_task_clear_dependencies_from_user_data (GTask    *task,
                                           GValue   *result,
                                           gpointer  user_data)
{
	GTask *complete_task;

	g_return_val_if_fail (G_IS_TASK (user_data), NULL);

	complete_task = G_TASK (user_data);
	g_task_clear_dependencies (complete_task);

	return NULL;
}

GTask*
g_task_any_of (GTask *first_task, ...)
{
	va_list  args;
	GTask   *task;
	GTask   *iter_task;

	if (first_task == NULL)
		return NULL;

	task = g_task_new (_g_task_dummy_func, NULL, NULL);
	iter_task = first_task;

	va_start (args, first_task);

	while (iter_task != NULL)
	{
		if (G_IS_TASK (iter_task))
		{
			g_task_add_dependency (task, iter_task);
			g_task_add_callback (iter_task,
			                     _g_task_clear_dependencies_from_user_data,
			                     g_object_ref (task),
			                     g_object_unref);
		}
		iter_task = va_arg (args, GTask*);
	}

	va_end (args);

	return task;
}

static void
g_task_get_property (GObject    *object,
                     guint       property_id,
                     GValue     *value,
                     GParamSpec *pspec)
{
	switch (property_id) {
	case PROP_STATE:
		g_value_set_enum (value, g_task_get_state (G_TASK (object)));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
	}
}

static void
g_task_set_property (GObject      *object,
                     guint         property_id,
                     const GValue *value,
                     GParamSpec   *pspec)
{
	switch (property_id) {
	case PROP_STATE:
		g_task_set_state (G_TASK (object), g_value_get_enum (value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
	}
}

static void
g_task_dispose (GObject *object)
{
	GTask *task;

	g_return_if_fail (G_IS_TASK (object));

	task = G_TASK (object);

	if (task->priv->previous)
	{
		g_object_unref (task->priv->previous);
		task->priv->previous = NULL;
	}

	if (G_LIKELY (task->priv->scheduler))
		g_object_unref (task->priv->scheduler);

	if (G_OBJECT_CLASS (g_task_parent_class)->dispose)
		G_OBJECT_CLASS (g_task_parent_class)->dispose (object);
}

static void
g_task_finalize (GObject *object)
{
	GTaskPrivate *priv;

	g_return_if_fail (G_IS_TASK (object));

	priv = G_TASK (object)->priv;

	g_mutex_lock (priv->mutex);

	if (priv->error)
		g_error_free (priv->error);

	g_list_foreach (priv->dependencies, (GFunc)g_object_unref, NULL);
	g_list_foreach (priv->handlers, (GFunc)_g_task_handler_free, NULL);

	g_list_free (priv->dependencies);
	g_list_free (priv->handlers);

	if (priv->previous)
		g_object_unref (priv->previous);

	if (G_VALUE_TYPE (&priv->result) != G_TYPE_INVALID)
		g_value_unset (&priv->result);

	g_mutex_unlock (priv->mutex);
	g_mutex_free (priv->mutex);

	G_OBJECT_CLASS (g_task_parent_class)->finalize (object);
}

static void
g_task_class_init (GTaskClass *task_class)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (task_class);

	g_type_class_add_private (task_class, sizeof (GTaskPrivate));

	task_class->execute         = _g_task_execute_real;
	gobject_class->get_property = g_task_get_property;
	gobject_class->set_property = g_task_set_property;
	gobject_class->dispose      = g_task_dispose;
	gobject_class->finalize     = g_task_finalize;

	/**
	 * GTask:state:
	 *
	 * The current task state.
	 */
	g_object_class_install_property (
		gobject_class,
		PROP_STATE,
		g_param_spec_enum ("state",
		                   "State",
		                   "Current task state",
		                   G_TYPE_TASK_STATE,
		                   G_TASK_READY,
		                   G_PARAM_READWRITE));

	/**
	 * GTask::state-changed:
	 * @task: the object which received the signal
	 * @state: the new #GTaskState
	 *
	 * The state-changed signal is emitted when a task moves into a new
	 * state.  This is particularly useful if you want to alter the state
	 * of dependent tasks when this task changes state.
	 */
	signals[STATE_CHANGED] =
		g_signal_new (g_intern_static_string ("state-changed"),
		              G_TYPE_FROM_CLASS (task_class),
		              G_SIGNAL_RUN_FIRST,
		              0, NULL, NULL,
		              g_cclosure_marshal_VOID__INT,
		              G_TYPE_NONE,
		              1, G_TYPE_INT);
}

static void
g_task_init (GTask *task)
{
	task->priv = G_TYPE_INSTANCE_GET_PRIVATE (task, G_TYPE_TASK, GTaskPrivate);
	task->priv->mutex = g_mutex_new ();
	task->priv->state = G_TASK_READY;
}

GType
g_task_state_get_type (void)
{
	static GType g_task_state_type_id = 0;
	if (G_UNLIKELY (g_task_state_type_id == 0))
	{
		static const GEnumValue values[] = {
			{G_TASK_WAITING,   "G_TASK_WAITING",   "waiting"},
			{G_TASK_READY,     "G_TASK_READY",     "ready"},
			{G_TASK_EXECUTING, "G_TASK_EXECUTING", "executing"},
			{G_TASK_CALLBACKS, "G_TASK_CALLBACKS", "callbacks"},
			{G_TASK_FINISHED,  "G_TASK_FINISHED",  "finished"},
			{G_TASK_CANCELLED, "G_TASK_CANCELLED", "cancelled"},
			{0, NULL, NULL}
		};
		g_task_state_type_id = g_enum_register_static ("GTaskState", values);
	}
	return g_task_state_type_id;
}
