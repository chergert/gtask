GTask is Asynchronous and Callback based execution for GObject.
It's kind of like co-routines, but not really.  About as much as
twisted is co-routines.  In fact, Twisted has a very special place
in my heart, and that will be seen in the code.

Unlike Twisted, however, GTask will work hard towards being multi-core
ready.  This is not to put twisted down, but it does have its limitations
due to the python GIL.  If you really want a good example of this, try to
service as many pages as possible using a static.File and twisted web.

In the process, we are somewhat removing the concept of the stack.  But
while doing so, we are hopefully giving a good api for developers to think
think about their problems a bit differently.

I've spent some time looking at the various different technologies
targeting multi-core utilization and for the most part, they are getting
there.  I havent seen any of them work towards getting you working on
execution chains between multiple languages (well other than .NET CCR by
nature of the runtime).

GTask will do that.  For example, I want a host envionrment that lets
me write code in python, ruby, and javascript, tune it when needed in
C, and finish it up with Mono.

The purpose is to build a framework for small chunks of processing and
defer blocking calls to threads until they are complete at which
time callbacks are initiated.  Of course, it would be rediculous to only
support blocking calls, so asynchronous tasks are also supported.  So if
your task wants to do an async read with gio or what-have-you, your
task can complete asynchronously during the read callback.

GTask has a few core concepts.

1.  GTaskFunc, GTaskCallback, and GTaskErrback

	These are delegates used to manage the lifecycle of the Task.

2.  GTask

	GTask provides a data structure for an task.  Tasks may have
	multiple callbacks associated with them.  Callbacks come in the form
	of regular callbacks or errbacks.  Errbacks jobs are to attempt to
	resolve an error so that further callback chains may continue.

3.  GTaskScheduler

	GTaskScheduler provides the indivudal scheduler component.  The
	default scheduler implementation uses GThreadPool to queue work
	items.  However, in the future, more schedulers should be
	implemented to support such things as work stealing between cores
	sharing sockets and what-not.

	The base task scheduler also provides a mechanism to dispatch
	callbacks through the applications main loop.  This is handy for UI
	developers in both gtk+ and clutter where altering UI primitives
	outside the main loop can cause drastic errors.  This feature is
	enabled by default and can be disabled via the "main-dispatch"
	property.

	Those interested in reaching high performance would want to
	consider disabling main-thread callbacks as well as to not use
	shared state between tasks.  This will help reduce memory barriers
	and lock contention between cpus.

	Someone really interested in high performance should look into
	writing a work-stealing scheduler.  This probably wont be useful
	until you can saturate the scheduling of tasks.

	I will be doing some where here shortly to pull the thread management
	outside of the scheduler.  This should make writing scheduler
	implementations much easier.

I would really like to see some ideas on how we can reduce the allocation of
memory for GTask objects.  Is there a way with gobject to have a pool of
memory pre-allocated in proper size chunks to be used for allocating a
particular object type?  If not, should we investigate not inheriting from
gobject and manage our own referencing and allocation pools?

Please see TODO for information on how we can improve things.
