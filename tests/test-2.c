#include <glib.h>
#include <gtask/gtask.h>

static GTask*
example (GTask *task, GValue *result, gpointer user_data)
{
	return NULL;
}

static void
gtask_ctor (void)
{
	gpointer a = g_task_new (example, NULL, NULL);
	g_assert (G_IS_TASK (a));
}

static void
gtask_dtor (void)
{
	GTask *a = g_task_new (example, NULL, NULL);
	g_assert (G_IS_TASK (a));
	g_object_unref (a);
}

static void
gtaskscheduler_ctor (void)
{
	gpointer a = g_task_scheduler_new ();
	g_assert (G_IS_TASK_SCHEDULER (a));
}

static void
gtaskscheduler_dtor (void)
{
	GTaskScheduler *a = g_task_scheduler_new ();
	g_assert (G_IS_TASK_SCHEDULER (a));
	g_object_unref (a);
}

static void
gtask_state (void)
{
	GTask *task = g_task_new (example, NULL, NULL);
	GTaskState state = G_TASK_WAITING;

	g_object_set (task, "state", G_TASK_CANCELLED, NULL);
	g_object_get (task, "state", &state, NULL);

	g_assert (state == G_TASK_CANCELLED);
}

int
main (int argc, char *argv[])
{
	g_type_init ();
	g_thread_init (NULL);
	g_test_init (&argc, &argv, NULL);

	g_test_add_func ("/object_ctor/gtask", gtask_ctor);
	g_test_add_func ("/object_ctor/gtaskscheduler", gtaskscheduler_ctor);
	g_test_add_func ("/object_dtor/gtask", gtask_dtor);
	g_test_add_func ("/object_dtor/gtaskscheduler", gtaskscheduler_dtor);

	g_test_add_func ("/object_props/gtask-state", gtask_state);

	return g_test_run ();
}
