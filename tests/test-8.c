#include <glib.h>
#include <gtask/gtask.h>
#include <stdlib.h>

static GMainLoop *main_loop = NULL;
static GMutex    *mutex     = NULL;
static gboolean   destroyed = FALSE;

static GTask*
func1 (GTask *task, GValue *result, gpointer user_data)
{
	return NULL;
}

static GTask*
dummy (GTask *task, GValue *result, gpointer user_data)
{
	return NULL;
}

static GTask*
quit (GTask *task, GValue *result, gpointer user_data)
{
	g_mutex_lock (mutex);
	g_assert (destroyed == TRUE);
	g_mutex_unlock (mutex);

	g_main_loop_quit (main_loop);

	return NULL;
}

static void
destroy (gpointer data)
{
	// This should get called when task2 unref's task1 during
	// remove dependency.

	g_mutex_lock (mutex);
	destroyed = TRUE;
	g_mutex_unlock (mutex);
}

static void
test1 (void)
{
	mutex = g_mutex_new ();

	GTaskScheduler *scheduler = g_task_scheduler_get_default ();

	GTask *task = g_task_new (func1, NULL, destroy);
	GTask *task2 = g_task_new (dummy, NULL, NULL);

	g_task_add_callback (task2, quit, NULL, NULL);
	g_task_add_dependency (task2, task);

	g_task_scheduler_schedule (scheduler, task);
	g_object_unref (task);

	g_task_scheduler_schedule (scheduler, task2);
	g_object_unref (task2);

 	main_loop = g_main_loop_new (NULL, FALSE);
	g_main_loop_run (main_loop);

	g_assert (destroyed == TRUE);
}

int
main (int argc, char *argv[])
{
	g_type_init ();
	g_thread_init (NULL);
	g_test_init (&argc, &argv, NULL);

	g_test_add_func ("/refcounts/destroyed1", test1);

	return g_test_run ();
}
