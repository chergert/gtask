#include <glib.h>

#include <gtask/gtask.h>

#include <stdlib.h>

GMainLoop *main_loop = NULL;

typedef struct {
	int counter;
} TestTaskState;

static GTask*
func1 (GTask *task, GValue *result, gpointer user_data)
{
	TestTaskState *state = user_data;
	g_assert_cmpint (state->counter, == , 1);
	state->counter++;

	return NULL;
}

static GTask*
func3 (GTask *task, GValue *result, gpointer user_data)
{
	TestTaskState *state = user_data;
	g_assert_cmpint (state->counter, == , 3);
	state->counter++;

	return NULL;
}

static GTask*
func2 (GTask *task, GValue *result, gpointer user_data)
{
	TestTaskState *state = user_data;
	g_assert_cmpint (state->counter, == , 2);
	state->counter++;

	return g_task_new (func3, state, NULL);
}

static GTask*
quit (GTask *task, GValue *result, gpointer user_data)
{
	TestTaskState *state = user_data;

	g_assert_cmpint (state->counter, == , 4);

	g_main_loop_quit (main_loop);

	return NULL;
}

static void
test1 (void)
{
	GTaskScheduler *scheduler = g_task_scheduler_new ();
	g_assert (G_IS_TASK_SCHEDULER (scheduler));

	TestTaskState *state = g_new0 (TestTaskState, 1);
	state->counter = 1;

	GTask *task = g_task_new (func1, state, NULL);

	g_task_add_callback (task, func2, state, NULL);
	g_task_add_callback (task, quit, state, NULL);

	g_task_scheduler_schedule (scheduler, task);

 	main_loop = g_main_loop_new (NULL, FALSE);
	g_main_loop_run (main_loop);
}

int
main (int argc, char *argv[])
{
	g_type_init ();
	g_thread_init (NULL);
	g_test_init (&argc, &argv, NULL);

	g_test_add_func ("/execution_order/1", test1);

	return g_test_run ();
}
