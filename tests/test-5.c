#include <glib.h>
#include <gtask/gtask.h>

static GMainLoop *main_loop = NULL;

static GTask*
test1_func (GTask *task, GValue *result, gpointer user_data)
{
	return NULL;
}

static GTask*
test1_cb (GTask *task, GValue *result, gpointer user_data)
{
	g_main_loop_quit (main_loop);
	return NULL;
}

static GTask*
test2_func (GTask *task, GValue *result, gpointer user_data)
{
	return NULL;
}

void
test1 (void)
{
	main_loop = g_main_loop_new (NULL, FALSE);

	GTaskScheduler *sched = g_task_scheduler_get_default ();

	GTask *task = g_task_new (test1_func, NULL, NULL);
	g_task_add_callback (task, test1_cb, NULL, NULL);

	GTask *task2 = g_task_new (test2_func, NULL, NULL);
	g_task_add_dependency (task, task2);

	g_assert (g_task_get_state (task) == G_TASK_WAITING);
	g_task_scheduler_schedule (sched, task);
	g_task_scheduler_schedule (sched, task2);

	g_object_unref (task);
	g_object_unref (task2);

	g_main_loop_run (main_loop);
}

int
main (int argc, char **argv)
{
	g_type_init (); 
	g_thread_init (NULL);
	g_test_init (&argc, &argv, NULL);

	g_test_add_func ("/delayed_start/1", test1);

	return g_test_run (); 
}
