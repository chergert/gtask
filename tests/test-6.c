#include <glib.h>
#include <gtask/gtask.h>

static GMainLoop *main_loop = NULL;

GTask*
test1_func (GTask *task, GValue *result, gpointer user_data)
{
	GHashTable *state = user_data;
	g_assert (state != NULL);

	g_hash_table_insert (state, g_strdup ("State 1"),
	                            g_strdup ("State 1 Value"));

	return NULL;
}

GTask*
test1_cb1 (GTask *task, GValue *result, gpointer user_data)
{
	GHashTable *state = user_data;
	g_assert (state != NULL);

	if (!g_hash_table_lookup (state, "State X"))
		g_task_set_error (task, g_error_new_literal (g_quark_from_static_string ("test"), 0, "Invalid state"));

	return NULL;
}

GTask*
test1_eb1 (GTask *task, const GError *error, GValue *result, gpointer user_data)
{
	g_assert (error != NULL);
	g_task_set_error (task, NULL);
	return NULL;
}

GTask*
test1_cb2 (GTask *task, GValue *result, gpointer user_data)
{
	GHashTable *state = user_data;
	g_assert (state != NULL);

	g_hash_table_remove (state, "State 1");

	return NULL;
}

GTask*
test1_fin (GTask *task, GValue *result, gpointer user_data)
{
	GHashTable *state = user_data;
	g_assert (state != NULL);
	g_assert (g_hash_table_size (state) == 0);
	g_main_loop_quit (main_loop);
	return NULL;
}

void
test1 (void)
{
	GHashTable *state;

	main_loop = g_main_loop_new (NULL, FALSE);
	state = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_free);

	GTask *task = g_task_new (test1_func, state, NULL);
	g_task_add_callback (task, test1_cb1, state, NULL);
	g_task_add_errback  (task, test1_eb1, state, NULL);
	g_task_add_callback (task, test1_cb2, state, NULL);
	g_task_add_callback (task, test1_fin, state, (GDestroyNotify)g_hash_table_destroy);
	g_task_scheduler_schedule (g_task_scheduler_get_default (), task);

	g_object_unref (task);

	g_main_loop_run (main_loop);
}

int
main (int argc, char *argv[])
{
	g_type_init ();
	g_thread_init (NULL);
	g_test_init (&argc, &argv, NULL);

	g_test_add_func ("/callbacks/1", test1);

	return g_test_run ();
}
