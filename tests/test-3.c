#include <glib.h>
#include <gtask/gtask.h>

static GMainLoop* loop = NULL;

static GTask*
quit (GTask *task, GValue *result, gpointer user_data)
{
	g_main_loop_quit (loop);
	return NULL;
}

static gboolean
timeout (gpointer data)
{
	GTask *task = data;

	g_task_set_state (task, G_TASK_CALLBACKS);
	g_object_unref (task);

	return FALSE;
}

static GTask*
func (GTask *task, GValue *result, gpointer user_data)
{
	// schedule callback in 1 msec, to mark the task finished
	g_timeout_add (1, timeout, g_object_ref (task));

	return NULL;
}

static void
async_test (void)
{
	loop = g_main_loop_new (NULL, FALSE);

	GTaskScheduler *sched = g_task_scheduler_new ();
	GTask *task = g_task_new (func, NULL, NULL);
	g_task_set_async (task, TRUE);
	g_task_add_callback (task, quit, NULL, NULL);
	g_task_scheduler_schedule (sched, task);

	g_object_unref (task);

	g_main_loop_run (loop);
}

int
main (int argc, char *argv[])
{
	g_type_init ();
	g_thread_init (NULL);
	g_test_init (&argc, &argv, NULL);

	g_test_add_func ("/async_task/basic", async_test);

	return g_test_run ();
}
