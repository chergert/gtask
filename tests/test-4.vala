/* gtask-test.vala
 *
 * Copyright (C) 2008 Christian Hergert <chris@dronelabs.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 
 * 02110-1301 USA
 */

using GLib;
using GTask;

namespace GTask {
	public class TestGTask: Object {
		public static void main (string[] args) {
			Test.init (ref args);
			Test.add_func ("/bindings/vala/test1", test_vala1);
			Test.run ();
		}

		public static void test_vala1 () {
			if (!Thread.supported ())
				error ("Threads not supported");
			new TestGTask ().run ();
		}

		MainLoop loop = new MainLoop (null, false);

		public void run () {
			Task task = new Task (t => {
				return null;
			});

			task.add_callback (t => {
				this.loop.quit ();
				return null;
			});

			TaskScheduler.get_default ().schedule (task);

			this.loop.run();
		}
	}
}
