#include <glib.h>
#include <gtask/gtask.h>

static GMainLoop *loop = NULL;
static gboolean dep_complete = FALSE;

static GTask*
verify_dep_complete (GTask    *task,
                     GValue   *result,
                     gpointer  user_data)
{
	g_assert (dep_complete == TRUE);
	return NULL;
}

static GTask*
set_dep_complete (GTask    *task,
                  GValue   *result,
                  gpointer  user_data)
{
	dep_complete = TRUE;
	return NULL;
}

static GTask*
quit_cb (GTask    *task,
         GValue   *result,
         gpointer  user_data)
{
	g_main_loop_quit (loop);
	return NULL;
}

static GTask*
dummy (GTask    *task,
       GValue   *result,
       gpointer  user_data)
{
	return NULL;
}

static void
test_deps (void)
{
	GTask *task, *dep;

	loop = g_main_loop_new (NULL, FALSE);

	dep = g_task_new (set_dep_complete, NULL, NULL);
	task = g_task_new (verify_dep_complete, NULL, NULL);

	g_task_add_callback (task, quit_cb, NULL, NULL);
	g_task_add_dependency (task, dep);

	g_task_scheduler_schedule (g_task_scheduler_get_default (), task);
	g_task_scheduler_schedule (g_task_scheduler_get_default (), dep);

	g_object_unref (task);
	g_object_unref (dep);

	g_main_loop_run (loop);
}

static void
test_deps2 (void)
{
	GTask *task, *dep;

	loop = g_main_loop_new (NULL, FALSE);

	dep = g_task_new (set_dep_complete, NULL, NULL);
	task = g_task_new (verify_dep_complete, NULL, NULL);

	g_task_add_callback (dep, dummy, NULL, NULL);
	g_task_add_callback (dep, dummy, NULL, NULL);
	g_task_add_callback (dep, dummy, NULL, NULL);
	g_task_add_callback (dep, dummy, NULL, NULL);
	g_task_add_callback (dep, dummy, NULL, NULL);
	g_task_add_callback (dep, dummy, NULL, NULL);
	g_task_add_callback (dep, dummy, NULL, NULL);
	g_task_add_callback (dep, dummy, NULL, NULL);
	g_task_add_callback (dep, dummy, NULL, NULL);
	g_task_add_callback (dep, dummy, NULL, NULL);
	g_task_add_callback (task, quit_cb, NULL, NULL);
	g_task_add_dependency (task, dep);

	g_task_scheduler_schedule (g_task_scheduler_get_default (), task);
	g_task_scheduler_schedule (g_task_scheduler_get_default (), dep);

	g_object_unref (dep);
	g_object_unref (task);

	g_main_loop_run (loop);
}

static void
test_deps3 (void)
{
	GTask *task, *dep, *dep2;

	loop = g_main_loop_new (NULL, FALSE);

	dep = g_task_new (set_dep_complete, NULL, NULL);
	dep2 = g_task_new (dummy, NULL, NULL);
	task = g_task_new (verify_dep_complete, NULL, NULL);

	g_task_add_callback (dep, dummy, NULL, NULL);
	g_task_add_callback (dep, dummy, NULL, NULL);
	g_task_add_callback (dep, dummy, NULL, NULL);
	g_task_add_callback (dep, dummy, NULL, NULL);
	g_task_add_callback (dep, dummy, NULL, NULL);
	g_task_add_callback (dep, dummy, NULL, NULL);
	g_task_add_callback (dep, dummy, NULL, NULL);
	g_task_add_callback (dep, dummy, NULL, NULL);
	g_task_add_callback (dep, dummy, NULL, NULL);
	g_task_add_callback (dep, dummy, NULL, NULL);
	g_task_add_callback (task, quit_cb, NULL, NULL);

	g_task_add_dependency (task, dep);
	g_task_add_dependency (dep, dep2);

	g_task_scheduler_schedule (g_task_scheduler_get_default (), task);
	g_task_scheduler_schedule (g_task_scheduler_get_default (), dep);
	g_task_scheduler_schedule (g_task_scheduler_get_default (), dep2);

	g_object_unref (task);
	g_object_unref (dep);

	g_main_loop_run (loop);
}

gint
main (gint argc, gchar *argv[])
{
	g_type_init ();
	g_thread_init (NULL);
	g_test_init (&argc, &argv, NULL);

	g_test_add_func ("/deps/single_dep", test_deps);
	g_test_add_func ("/deps/single_dep_with_callbacks", test_deps2);
	g_test_add_func ("/deps/recursive_deps_with_callbacks", test_deps3);

	return g_test_run ();
}
