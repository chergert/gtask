#include <glib.h>
#include <gtask/gtask.h>
#include <stdlib.h>

static GMainLoop *main_loop = NULL;
static gboolean done = FALSE;

static GTask*
func1 (GTask *task, GValue *result, gpointer user_data)
{
  done = TRUE;
	return NULL;
}

static GTask*
dummy (GTask *task, GValue *result, gpointer user_data)
{
	return NULL;
}

static GTask*
quit (GTask *task, GValue *result, gpointer user_data)
{
  g_assert (done == TRUE);
	g_main_loop_quit (main_loop);
	return NULL;
}

static void
test1 (void)
{
	GTaskScheduler *scheduler = g_task_scheduler_get_default ();

	GTask *task = g_task_new (func1, NULL, NULL);
	GTask *task2 = g_task_new (dummy, NULL, NULL);
	GTask *done = g_task_all_of (task, task2, NULL);

	g_task_add_callback (done, quit, NULL, NULL);
	g_task_add_dependency (task2, task);

	g_task_scheduler_schedule (scheduler, task2);
	g_task_scheduler_schedule (scheduler, done);
	g_task_scheduler_schedule (scheduler, task);

	g_object_unref (task);
	g_object_unref (task2);
	g_object_unref (done);

 	main_loop = g_main_loop_new (NULL, FALSE);
	g_main_loop_run (main_loop);
}

int
main (int argc, char *argv[])
{
	g_type_init ();
	g_thread_init (NULL);
	g_test_init (&argc, &argv, NULL);

	g_test_add_func ("/g_task_all_of/1", test1);

	return g_test_run ();
}
